package TpPilas;
/**
 * Created by SebasBelaustegui on 3/28/17.
 * Stack test.
 */
public class StackTest {

    public static void main(String[] args) {
        DynamicStack<Integer> ints = new DynamicStack<>();
        ints.push(2);
        System.out.println(ints.size());
        ints.push(4);
        System.out.println(ints.size());
        ints.pop();
        System.out.println(ints.size());
        ints.push(3);
        ints.push(4);
        System.out.println(ints.isEmpty());
        System.out.println(ints.peek());
        System.out.println(ints.size());
        ints.empty();
        System.out.println(ints.size());
        System.out.println(ints.isEmpty());
        ints.peek();
        ints.pop();
    }
}
