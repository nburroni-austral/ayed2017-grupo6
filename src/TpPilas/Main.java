package TpPilas; /**
 * Created by JJaramil on 3/28/17.
 */

import javax.script.ScriptException;
import java.util.Scanner;
/**
    For testing purpuse only. Parking Lot test.
 */

public class Main {

        public static void main(String[] args) {

            ParkingLot parking = new ParkingLot();

            parking.addCar(new Car("1", "piqe"));
            parking.addCar(new Car("2", "piqwe"));
            parking.addCar(new Car("3", "piqwq"));
            parking.addCar(new Car("4", "pisqe"));
            parking.addCar(new Car("5", "fspiq"));
            parking.addCar(new Car("6", "pisfsq"));
            parking.addCar(new Car("7", "pisswhjq"));
            parking.addCar(new Car("8", "piqyuyu"));
            parking.addCar(new Car("9", "piquyuyu7"));
            parking.addCar(new Car("10", "piqkjh"));

            parking.printParking();

            parking.removeCar("6");

            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");

            parking.printParking();

            System.out.println("");
            System.out.println("Parking Incomes:  " + parking.getIncome());
            System.out.println("");
            Calculator calc = new Calculator();
            System.out.println("Calculator: ");
            System.out.println("");
            System.out.println("1+7: " + calc.calculate("1+7"));
            System.out.println("");
            System.out.println("1*1+5*3+5/8-3*4/5*8*6-7: " + calc.calculate("1*1+5*3+5/8-3*4/5*8*6-7"));
        }
}


