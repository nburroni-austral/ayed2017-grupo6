package TpPilas;

import struct.Stack;

/**
 * Created by SebasBelaustegui on 3/22/17.
 * Dynamic Stack class.
 */
public class DynamicStack<T> implements Stack<T> {

    int size;
    Nodo top;

    public DynamicStack() {
        top = new Nodo(null, null);
        size = 1;
    }

    @Override
    public void push(T info) {
        Nodo newNodo = new Nodo(null,info);
        top.next = newNodo;
        top = newNodo;
        size++;
    }

    @Override
    public void pop() {
        if(!isEmpty()){
            top = top.next;
            size--;
        }
    }

    @Override
    public T peek() {
        return (T) top.info;
    }

    @Override
    public boolean isEmpty() {
        return (top == null);
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public void empty() {
        top = null;
    }
}