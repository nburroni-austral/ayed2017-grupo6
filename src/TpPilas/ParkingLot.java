package TpPilas;
/**
 * Created by JJaramil on 3/28/17.
 */
public class ParkingLot {

    private StaticStack parking;
    private StaticStack sidewalk;
    private double income;

    /**
     * Builds a parking Lot with capacity for 50 cars. With a sidewalk of the same size to remove cars.
     */
    public ParkingLot() {
        this.parking = new StaticStack(50);
        this.sidewalk = new StaticStack(50);
        this.income = 0;
    }



    void addCar(Car c){
        parking.push(c);
    }

    public double getIncome() {
        return income;
    }

    /**
     * Pops cars from stack and push them into sidewalk until peeked car has the same license plate as the requested car.
     * Returns Car and increments income in 5.
     * @param licensePlate
     * @return removedCar
     */
    Car removeCar(String licensePlate){
        Car removedCar = null;
        int top = parking.size();
        for (int i = 0; i<=top;i++){
            Car firstCar = (Car)parking.peek();
            if(firstCar.getLicensePlate().equals(licensePlate)){
                removedCar = firstCar;
                income +=5;
                parking.pop();
            }else{
            sidewalk.push(firstCar);
            parking.pop();
            }
        }
        parking = sidewalk;
        return removedCar;
    }

    /**
     * For testing purposes, prints the parking lot, License plata and then model.
     */
    void printParking(){
        int top = parking.size();
        StaticStack temp = new StaticStack(parking.size());
        for(int i =0; i<=top; i++){
            temp.push(parking.peek());
            Car car = (Car) temp.peek();
            System.out.println(car != null ? car.getLicensePlate()+"  y  modelo: "+car.getModel(): "no hay auto");
            parking.pop();
        }
        for(int i =0; i<=top; i++){
            Car car  = (Car) temp.peek();
            parking.push(car);
            temp.pop();
        }
    }



}
