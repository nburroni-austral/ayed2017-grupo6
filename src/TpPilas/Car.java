package TpPilas;
/**
 * Class Car used in ParkingLot class.
 */
public class Car {

    protected String licensePlate;
    protected String model;

    public Car(String licensePlate, String model) {
        this.licensePlate = licensePlate;
        this.model = model;
    }

    public String getLicensePlate() {
        return licensePlate;
    }
    public String getModel() {
        return model;
    }

}
