package TpPilas;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
/**
 * Lexicographic Analyzer class. It checks in a txt file if tu miss {}, () or [].
 */
public class LexicographicAnalyzer {


    private static StaticStack<Character> parenthesisStack = new StaticStack<>(10);
    private static StaticStack<Character> bracketsStack = new StaticStack<>(10);
    private static StaticStack<Character> bracesStack = new StaticStack<>(10);

    public static void main(String[] args) {
        try {
            readFile("Ejemplo.txt");
        } catch (IOException e) {
            System.out.println("No se encuentra el archivo.");
        }
        resultsPrinter();
    }

    /**
     * Read File method, reads the file and puts on a different stack the simbols.
     * @param filename
     * @throws IOException
     */
    private static void readFile(String filename) throws IOException {
        String line;

        FileReader file = new FileReader(filename);
        BufferedReader scanner = new BufferedReader(file);
        while((line = scanner.readLine())!=null) {
            char[] characters = line.toCharArray();
            for (int i = 0; i < characters.length; i++) {
                if(characters[i] == '{' || characters[i] == '}'){
                    bracesStack.push(characters[i]);
                }else if(characters[i] == '(' || characters[i] == ')') {
                    parenthesisStack.push(characters[i]);
                }else if(characters[i] == '[' || characters[i] == ']'){
                    bracketsStack.push(characters[i]);
                }
            }
        }
        scanner.close();
    }

    /**
     *
     * @param a
     * @param b
     * @param stack
     * @return true if its equal the number of simbols 'a' with simbols 'b'. False if its not.
     */
    private static boolean counter(char a, char b, StaticStack<Character> stack){

        int openers = 0;
        int closers = 0;

        while(!stack.isEmpty()) {
            if(stack.peek() == a){
                openers++;
                stack.pop();
            }else if(stack.peek() == b){
                closers++;
                stack.pop();
            }
            else {
                stack.pop();
            }
        }
        return (openers == closers);
    }

    /**
     * Prints the results.
     */
    private static void resultsPrinter(){
        if (counter('{','}',bracesStack)){
            System.out.println("Las llaves están bien.");
        }else{
            System.out.println("Faltan o sobran llaves.");
        }
        if (counter('(',')',parenthesisStack)){
            System.out.println("Los paréntesis están bien.");
        }else{
            System.out.println("Faltan o sobran paréntesis.");
        }
        if (counter('[',']',bracketsStack)){
            System.out.println("Los corchetes están bien.");
        }else{
            System.out.println("Faltan o sobran corchetes.");
        }
    }
}
