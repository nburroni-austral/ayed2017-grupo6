package TpPilas;
/**
 * Created by SebasBelaustegui on 3/28/17.
 * Nodo Class for stacks.
 */
public class Nodo<T>{

    Nodo next;
    T info;

    public Nodo(T info){
        this.info = info;
    }

    public Nodo(Nodo nodo, T info){
        this.next = nodo;
        this.info = info;
    }
}