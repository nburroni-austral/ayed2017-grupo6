package TpPilas;

import java.util.ArrayList;
import static java.lang.Character.isDigit;

/**
 * Created by JJaramil on 3/28/17.
 */

public class Calculator {

    /**
     * Creates a stack with strings alternating with + or - signs, this stack can contain a number or a string
     * containing operations of multiplication and division, then to preserve operation's logic, reverses the stack.
     * The next step will be use the calString method. to obtain the results of every string whether it has one number
     * or more. Then it sums or subtract each element of the stack in correct order.
     * @param s
     * @return Result of s internal operations.
     */
    public double calculate(String s)  {


        StaticStack firstStack = new StaticStack(50);
        int len = s.length();
        String temp = "";
        /**
         * adding elements to first stack, this elements will be in reverse order.
         */
        for (int i = 0; i < len; i++) {
            if(isDigit(s.charAt(i))||s.charAt(i)=='*'||s.charAt(i) == '/'){
                temp = temp+""+s.charAt(i);
            }else{
                char symbol = s.charAt(i);
                firstStack.push(temp);
                temp="";
                firstStack.push(symbol);
            }
        }
        firstStack.push(temp);

        /**
         * reversing first stack
         */
        StaticStack reverseStack = new StaticStack(firstStack.size());
        while(0<=firstStack.size()){
            reverseStack.push(firstStack.peek());
            firstStack.pop();
        }

        /**
         * getting first string and first operand, then adding or subtracting depending on the sign with the next
         * calcString(element).
         */
        String first = (String)reverseStack.peek();
        double result = calcString(first);
        reverseStack.pop();
        char Operand = (char)reverseStack.peek();
        reverseStack.pop();

        while(0<=reverseStack.size()){
            String x = (String)reverseStack.peek();
            if(Operand == '+'){
                result = result + calcString(x);
                reverseStack.pop();
            }else{
                result = result - calcString(x);
                reverseStack.pop();
            }
            if(reverseStack.size()>=0) {
                Operand = (char) reverseStack.peek();
                reverseStack.pop();
            }
        }



        return result;
    }


    /**
     * receiving string that contains only multiplication and division operations, forms two lists, one with numbers and
     * the other one with operands. Then, depending on the sign it makes the correspondent operation.
     * @param x
     * @return result of every operation on String x.
     */
    public double calcString(String x){

        int len2 = x.length();
        ArrayList list1 = new ArrayList();
        ArrayList list2 = new ArrayList();

        String temp2 = "";
        for (int i = 0; i < len2; i++) {
            if (isDigit(x.charAt(i))) {
                temp2 = temp2 + x.charAt(i);
            } else {
                list1.add(Double.valueOf(temp2));
                temp2="";
                list2.add(x.charAt(i));
            }
        }
        list1.add(Double.valueOf(temp2));


        double result=(double)list1.get(0);
        for (int t = 0; t < list2.size(); t++) {
            char oper = (char) list2.get(t);
            if (oper == '*') {
                result =  result * (double)list1.get(t+1) ;

            } else if (oper == '/') {
                result = result / (double)list1.get(t+1) ;
            }
        }
        return result;
    }
}