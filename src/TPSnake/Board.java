package TPSnake;

import java.util.ArrayList;

/**
 * Created by Sebas Belaustegui on 4/12/17.
 */
public class Board {

    private int n;
    private ArrayList<Cell> board;

    public Board(int n){
        this.n = n;
        board = new ArrayList<>();
        for (int i = 0; i < (n*n); i++) {
            board.add(new Cell(i, false));
        }
        makePath();
    }

    private void makePath(){
        for (Cell cell: board) {
            if(cell.getIndex() == n){
                for (int i = n; i < n+(n-2); i++) {
                    board.get(i).setFree(true);
                }
                for (int i = n+n-2; i < (n*n-1)-(n*2-2); i+=n) {
                    board.get(i).setFree(true);
                }
                for (int i = (n*n-1)-(n*2-2); i < n*n-1-(2*n); i++) {
                    board.get(i).setFree(true);
                }
            }
        }
    }

//    private void solvePath(){
//        for (Cell cell: board) {
//
//        }
//    }



    public void printBoard() {
//        System.out.println(" ----+----+----+----+----+----+----+----+----+");
        for (int i = 0; i <= ((n * n) - 1); i += n) {
            for (int j = i; j <= (i + n)-1; j++) {
                String toPrint = board.get(j).isFree() ? " " : "B";
                String boardString = "    | " + toPrint + " | ";
                System.out.print(boardString);
                if(j==(n-1)){
                    System.out.println("");
                }
                if(j == i && j!=0){
                    System.out.println("" );
                }
            }
        }
    }



}
