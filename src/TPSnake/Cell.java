package TPSnake;

/**
 * Created by Sebas Belaustegui on 4/12/17.
 */
public class Cell {

    private int index;
    private boolean free;

    public Cell(int index, boolean free) {
        this.index = index;
        this.free = free;
    }

    public int getIndex() {
        return index;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }
}
