package arbol234;

/**
 * Created by Sebas Belaustegui on 6/7/17.
 */
class Node{
    private static final int ORDER = 4;
    private int numberOfItems;
    private Node parent;
    private Node childArray[] = new Node[ORDER];
    private String itemArray[] = new String[ORDER-1];

    public void attachChild(int childNumber, Node child){
        childArray[childNumber] = child;
        if(child != null)
            child.parent = this;
    }

    public Node desAttachChild(int childNumber){
        Node aux = childArray[childNumber];
        childArray[childNumber] = null;
        return aux;
    }

    public Node getChild(int childNumber){
        return childArray[childNumber]; 
    }

    public Node getParent(){
        return parent; 
    }

    public boolean isLeaf(){
        return childArray[0]==null;
    }

    public int getNumberOfItems(){
        return numberOfItems; 
    }

    public String getItem(int index){
        return itemArray[index];
    }

    public boolean isFull(){
        return numberOfItems ==ORDER-1;
    }

    public int findItem(String key){
        for(int j=0; j<ORDER-1; j++){
            if(itemArray[j] == null)
                break;
            else if(itemArray[j].equals(key))
                return j;
        }
        return -1;
    }

    public int insertItem(String newItem){
        numberOfItems++;
        String newKey = newItem;

        for(int j=ORDER-2; j>=0; j--){
            if(itemArray[j] == null)
                continue;
            else{
                String itsKey = itemArray[j];
                if(newKey.compareTo(itsKey)<0)
                    itemArray[j+1] = itemArray[j];
                else{
                    itemArray[j+1] = newItem;
                    return j+1;
                }
            }
        }
        itemArray[0] = newItem;
        return 0;
    }
    
    public String removeItem(){
        String temp = itemArray[numberOfItems -1];  
        itemArray[numberOfItems -1] = null;           
        numberOfItems--;                             
        return temp;                            
    }

    public void displayNode(){
        for(int j=0; j<numberOfItems; j++)
            System.out.print("/"+itemArray[j]);
        System.out.println("/");
    }

}
