package arbol234;

import java.util.Scanner;

/**
 * Created by Sebas Belaustegui on 6/7/17.
 */
public class main {

    static Tree234 DNITree = new Tree234();

    public static void main(String[] args){
        askForDNI();
    }

    public static void askForDNI(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese un número de DNI:");
        String dni = scanner.next();
        if(dni.equals("0")){
            DNITree.displayTree();
            System.exit(0);
        }else{
            DNITree.insert(dni);
            askForDNI();
        }
    }

}
