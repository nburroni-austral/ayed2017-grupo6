package arbol234;

/**
 * Created by Sebas Belaustegui on 6/7/17.
 */
class Tree234{
    
    private Node root = new Node();

    public int find(String key){
        Node root = this.root;
        int childNumber;
        while(true){
            if((childNumber=root.findItem(key) ) != -1)
                return childNumber;
            else if(root.isLeaf() )
                return -1;
            else
                root = getNextChild(root, key);
        }
    }

    public void insert(String dValue){
        Node curNode = root;
        String tempItem = new String(dValue);

        while(true){
            if(curNode.isFull() ){
                split(curNode);
                curNode = curNode.getParent();
                curNode = getNextChild(curNode, dValue);
            }else if(curNode.isLeaf() )
                break;
            else
                curNode = getNextChild(curNode, dValue);
        }

        curNode.insertItem(tempItem);
    }

    public void split(Node thisNode){
        String itemB, itemC;
        Node parent, child2, child3;
        int itemIndex;

        itemC = thisNode.removeItem();
        itemB = thisNode.removeItem();
        child2 = thisNode.desAttachChild(2);
        child3 = thisNode.desAttachChild(3);

        Node newRight = new Node();

        if(thisNode==root){
            root = new Node();
            parent = root;
            root.attachChild(0, thisNode);
        }else                             
            parent = thisNode.getParent();
        
        itemIndex = parent.insertItem(itemB);
        int n = parent.getNumberOfItems();     

        for(int j=n-1; j>itemIndex; j--){                                     
            Node temp = parent.desAttachChild(j);
            parent.attachChild(j+1, temp);
        }
       
        parent.attachChild(itemIndex+1, newRight);
        
        newRight.insertItem(itemC);       
        newRight.attachChild(0, child2); 
        newRight.attachChild(1, child3);
    }
    
    public Node getNextChild(Node theNode, String theValue){
        int j;
        int numItems = theNode.getNumberOfItems();
        for(j=0; j<numItems; j++){                            
            if(theValue.compareTo(theNode.getItem(j)) < -1)
                return theNode.getChild(j);
        }                 
        return theNode.getChild(j);       
    }

    public void displayTree(){
        recDisplayTree(root, 0, 0);
    }

    private void recDisplayTree(Node thisNode, int level, int childNumber){
        System.out.print("Nivel="+level+"- Hijo="+childNumber+": ");
        thisNode.displayNode();

        int numItems = thisNode.getNumberOfItems();
        for(int j=0; j<numItems+1; j++){
            Node nextNode = thisNode.getChild(j);
            if(nextNode != null)
                recDisplayTree(nextNode, level+1, j);
            else
                return;
        }
    }
}