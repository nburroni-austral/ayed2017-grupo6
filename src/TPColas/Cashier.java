package TPColas;

import Queue.DynamicQueue;

/**
 * Created by Sebas Belaustegui on 4/11/17.
 */
public class Cashier {

    String id;
    private double serveTime;
    private DynamicQueue<Client> clients;
    boolean isFree;



    public Cashier(String id, double serveTime) {
        this.id = id;
        this.serveTime = serveTime;
    }

     DynamicQueue<Client> getClients() {
        return clients;
    }

    /**
     * Initializes the client queue of the cashier.
     */
     void openQueue(){
        clients = new DynamicQueue<Client>();

    }

    public double getServeTime() {
        return serveTime;
    }

    DynamicQueue getQueue(){
         return clients;
    }

     boolean isFree(){
        return isFree;
    }

     void serve(){
         isFree=false;
    }

     void free(){
         isFree=true;
    }
}
