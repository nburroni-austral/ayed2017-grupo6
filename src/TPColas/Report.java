package TPColas;

/**
 * Created by JJaramil on 4/13/17.
 */
public class Report {

    private int clientsServed;
    private int clientsRefused;

    public Report() {
        this.clientsServed = 0;
        this.clientsRefused = 0;
    }


    public int getClientsRefused() {
        return clientsRefused;
    }

    /**
     * increments the amount of refused clients
     */
    public void refuseClient(){
        clientsRefused += 1;
    }

    public int getClientsServed() {
        return clientsServed;
    }


    /**
     * increments the amount of clients served
     */
    public void serveClient(){
        clientsServed += 1;
    }

}
