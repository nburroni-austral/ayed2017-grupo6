package TPColas;
import Queue.DynamicQueue;

import java.util.ArrayList;

/**
 * Created by Sebas Belaustegui on 4/11/17.
 */
public class Bank {

    ArrayList<Cashier> bankCashiers;
    Controller controller;
    long OPEN_TIME = 1000000;
    long CLOSE_TIME = 1500000;
    Report report;
    long currentTimeMins;
    long closeTimeMins;
    DynamicQueue<Client> clients;
    boolean isDoorOpen;

    public Bank(Controller controller) {
        this.bankCashiers = bankCashiers;
        this.controller = controller;
    }

    /**
     * Opens door and initializes Clients queues and cashiers with serveTime
     */
    void openBank( ){
        isDoorOpen = true;
        report = new Report();
        currentTimeMins = (OPEN_TIME/1000)/60;
        closeTimeMins = (CLOSE_TIME/1000)/60;
            clients = new DynamicQueue<Client>();
            for (Cashier cashier: bankCashiers) {
                cashier.openQueue();
        }
        bankCashiers.add(new Cashier("1",1));
        bankCashiers.add(new Cashier("2", 1.5));
        bankCashiers.add(new Cashier("3", 2));
    }

    /**
     * If door is closed and there are no clients, make report
     */
    void closeDoor(){
        isDoorOpen = false;
        if(! areThereClients()) makeReport();
    }

    /**
     * when door is closed and there are still clients, the bank end its operations when the last client left.
     */
    void endBankOperations(){
     if(! areThereClients()) makeReport();
    }

    /**
     * chackes if any queue has clients.
     * @return clients in the bank
     */
    boolean areThereClients(){
            if(clients.isEmpty()){
               return true;
            }
            int i = 0;
            for (Cashier cashier:bankCashiers) {
                if (cashier.getClients().isEmpty()){
                    i +=1;
                }
            }
            if (i == bankCashiers.size())return true;

        return false;
    }


    /**
     * admits clients if they do not leave deppending on the client refuse probability.
     * @param client
     * @return client queued or not
     */
    public boolean acceptClient(Client client){
        if(! clients.isEmpty()){
            return clientRefuseProbability(clients, client);
        }
        boolean result = false;
        for ( Cashier cashier : bankCashiers ) {
            result = clientRefuseProbability(cashier.getClients(),client);
        }
        return result;

    }

    /**
     * depending on the amount of clients in clients calculates the probability of that client staying
     * @param clients
     * @param client
     * @return if client decide to stay or not
     */
    public boolean clientRefuseProbability(DynamicQueue clients, Client client) {
        if(clients.size()<3){
            clients.enqueue(client);
            return true;
             }else if(clients.size()>3 && clients.size()<=8){
            if(Math.random()<=0.25){
                clients.enqueue(client);
                return true;
            }
        }else if(clients.size()>8){
            if(Math.random()<=0.5){
                clients.enqueue(client);
                return true;
            }
        }
            return false;
    }

    /**
     * Assign client to cashier deppending on the strategy of the parameter and if any cashier is free.
     * @param strategy
     * @return if the client is assigned to a cashier.
     */
    public boolean assignClient(Strategy strategy){
        ArrayList<DynamicQueue> queues = new ArrayList<>();
        if(strategy instanceof StrategyA){
            queues.add(clients);
            long serveTime = (long)strategy.assignClient(bankCashiers,queues);
            currentTimeMins = (currentTimeMins)+serveTime;
            report.serveClient();
            if(currentTimeMins == closeTimeMins)closeDoor();
            return true;
        }else{
            for (Cashier cashier: bankCashiers ) {
                queues.add(cashier.getQueue());
            }
            long serveTime = (long)strategy.assignClient(bankCashiers,queues);
            currentTimeMins = (currentTimeMins)+serveTime;
            report.serveClient();
            if(currentTimeMins == closeTimeMins)closeDoor();
            return true;
        }
    }

    /**
     * makes report
     */
    void makeReport(){
        System.out.println("Amount of served clients: "+report.getClientsServed());
        System.out.println("Amount of refused clients: "+report.getClientsRefused());
    }




}
