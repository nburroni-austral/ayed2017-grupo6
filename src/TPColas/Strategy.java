package TPColas;

import Queue.DynamicQueue;

import java.util.ArrayList;

/**
 * Created by JJaramil on 4/11/17.
 */
public interface Strategy {

    double assignClient(ArrayList<Cashier> cashiers, ArrayList<DynamicQueue> queues);
}

