package TPColas;

import Queue.Queue;

import java.util.Comparator;

public class PriorityQueue<Q> implements Queue<Q> {

    Object[] queue;
    Comparator<Q> comparator;
    int size;

    public PriorityQueue(int initialCapacity, Comparator<Q> comparator) {
        this.queue = new Object[initialCapacity];
        this.comparator = comparator;
    }

    @Override
    public void enqueue(Q q) {
        int i = size;
        size = i + 1;
        if (i == 0) {
            queue[0] = q;
        }else {
            while (i > 0) {
                int parent = (i - 1) >>> 1;
                Object e = queue[parent];
                if (comparator.compare(q, (Q) e) >= 0) {
                    break;
                }
                queue[i] = e;
                i = parent;
            }
            queue[i] = q;
        }
    }

    @Override
    public Q dequeue() {
        if (size == 0)
            return null;
        int s = --size;
        Q result = (Q) queue[0];
        Q x = (Q) queue[s];
        queue[s] = null;
        if (s != 0){
            int k = 0;
            int half = size >>> 1;
            while (k < half) {
                int child = (k << 1) + 1;
                Object c = queue[child];
                int right = child + 1;
                if (right < size &&
                        comparator.compare((Q) c, (Q) queue[right]) > 0)
                    c = queue[child = right];
                if (comparator.compare(x, (Q) c) <= 0)
                    break;
                queue[k] = c;
                k = child;
            }
            queue[k] = x;
        }
        return result;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int length() {
        return queue.length;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        for (int i = 0; i < size; i++)
            queue[i] = null;
        size = 0;
    }
}
