package TPColas;

public class PriorityQueueTest {
    public static void main(String[] args) {
        PriorityQueue<Integer> numbersQueue = new PriorityQueue<>(10, (o1, o2) -> {
            if(o1 < o2){
                return 1;
            }else if(o2 == o1){
                return 0;
            }else{
                return -1;
            }
        });

        numbersQueue.enqueue(2);
        numbersQueue.enqueue(1);
        numbersQueue.enqueue(8);
        numbersQueue.enqueue(3);
        numbersQueue.enqueue(2);
        numbersQueue.enqueue(12);
        numbersQueue.enqueue(7);
        numbersQueue.enqueue(3);
        numbersQueue.enqueue(9);
        numbersQueue.enqueue(12);

        for (int i = 0; i < 10; i++) {
            System.out.println(String.valueOf(numbersQueue.dequeue()));
        }
    }
}
