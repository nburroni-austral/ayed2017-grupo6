package TPColas;
import Queue.DynamicQueue;
import TpPilas.StaticStack;

import java.util.Scanner;

/**
 * Created by Sebas Belaustegui on 4/13/17.
 */
public class Palindrome {
    public static void main(String[] args) {
        System.out.println("Ingrese palabra:");
        Scanner a = new Scanner(System.in);
        if (isPalindrome(a.next())){
            System.out.println("La palabra es capicúa.");
        }else {
            System.out.println("La palabra no es capicúa");
        }
    }

    private static boolean isPalindrome(String word){
        StaticStack<Character> wordStack = new StaticStack<>(word.length());
        DynamicQueue<Character> wordQueue = new DynamicQueue<Character>();
        for (int i = 0; i < word.length(); i++) {
            wordStack.push(word.charAt(i));
            wordQueue.enqueue(word.charAt(i));
        }
        for (int i = 0; i < word.length(); i++) {
            if (wordStack.peek()!=wordQueue.dequeue()){
                return false;
            }
            wordStack.pop();
        }
        return true;
    }
}
