package TPColas;

import Queue.DynamicQueue;

import java.util.ArrayList;

/**
 * Created by JJaramil on 4/11/17.
 */
public class StrategyA implements Strategy {

    /**
     * Works with only one queue, get the amount of free cashiers and assign the client randomly,
     * if there is only one cashier free then it assigns client to that cashier.
     * @param cashiers
     * @param queues
     * @return servetime of the assign cashier.
     */
    @Override
    public double assignClient(ArrayList<Cashier> cashiers, ArrayList<DynamicQueue> queues) {
        ArrayList<Cashier> freecashiers = new ArrayList<>();
        for (Cashier c : cashiers) {
            if(c.isFree())freecashiers.add(c);
        }
        if(freecashiers.size()>=1) {
            Cashier cashier = freecashiers.get((int) (Math.random() * freecashiers.size() - 1));
            cashier.serve();
            queues.get(0).dequeue();
            return (int) (Math.random() * cashier.getServeTime())+0.5;
        }else {
            return 0;
        }
    }
}
