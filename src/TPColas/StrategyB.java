package TPColas;

import Queue.DynamicQueue;

import java.util.ArrayList;

/**
 * Created by JJaramil on 4/12/17.
 */
public class StrategyB implements Strategy {

    /**
     * Works with all the queues of the cashiers, get the amount of free cashiers and empty queues,  assign the client randomly,
     * if there is only one cashier free then it assigns client to that cashier.
     * @param cashiers
     * @param queues
     * @return servetime of the assign cashier.
     */
    @Override
    public double assignClient(ArrayList<Cashier> cashiers, ArrayList<DynamicQueue> queues) {
        ArrayList<Cashier> freeCashiers = new ArrayList<>();
        for (Cashier cashier : cashiers) {
            if(cashier.isFree()) {
                freeCashiers.add(cashier);
            }
        }
        ArrayList<DynamicQueue> notEmptyQueues = new ArrayList<>();
        for (DynamicQueue queue: queues ) {
            if(!queue.isEmpty()){
                notEmptyQueues.add(queue);
            }
        }
        if(freeCashiers.size()>=1 && notEmptyQueues.size()<=freeCashiers.size()){
            if(notEmptyQueues.size()>=1){
                Cashier cashier = freeCashiers.get((int) (Math.random() * freeCashiers.size() - 1));
                        cashier.serve();
                notEmptyQueues.get((int) (Math.random() * notEmptyQueues.size() - 1)).dequeue();
                return (int) (Math.random() * cashier.getServeTime())+0.5;
            }
        }
        return 0;
    }
}
