package Queue;

/**
 * Created by Sebas Belaustegui on 6/4/2017.
 */
public class DynamicQueue<Q> implements Queue<Q>{
    private Node frontNode;
    private Node backNode;
    private int size;

    public DynamicQueue() {
        size = 0;
    }

    public void enqueue(Q q) {
        if(size == 0){
            frontNode = new Node(q);
            backNode = frontNode;
            size++;
        }else{
            Node aux = new Node(q);
            backNode.next = aux;
            backNode = aux;
            size++;
        }
    }

    public Q dequeue() {
        if(!isEmpty()){
            size--;
            Node aux = new Node(frontNode.getObject());
            frontNode = frontNode.next;
            return (Q) aux.getObject();
        }
        return null;
    }

    public boolean isEmpty() {
        if (size==0)
            return true;
        return false;
    }

    public int length() {
        return 0;
    }

    public int size() {
        return size;
    }

    public void empty() {
        size=0;
    }
}
