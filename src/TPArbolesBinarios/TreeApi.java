package TPArbolesBinarios;

import java.io.*;
import java.util.ArrayList;

import java.util.*;

/**
 * Created by Sebas Belaustegui on 4/4/17.
 */
public class TreeApi <T>{

    //Ejercicio 13.A
    public int size (BinaryTreeImpl<T> a){
        if(a.isEmpty())
            return 0;
        else
            return 1 + size(a.getLeft())+size(a.getRight());
    }

    //Ejercicio 13.B
    public int completeNodes(BinaryTreeImpl<T> a){
        if(a.isEmpty())
            return 0;
        if (a.getLeft().isEmpty())
            return completeNodes(a.getRight());
        if (a.getRight().isEmpty())
            return completeNodes(a.getLeft());
        return 1+completeNodes(a.getRight())+completeNodes(a.getLeft());
    }

    //Ejercicio 13.C
    public int ocurrencies(BinaryTreeImpl<T> a, T o){
        if(a.isEmpty())
            return 0;
        if(a.getRoot().equals(o))
            return 1 + ocurrencies(a.getLeft(),o)+ ocurrencies(a.getRight(),o);
        else
            return ocurrencies(a.getLeft(),o)+ ocurrencies(a.getRight(),o);
    }

    //Ejercicio 13.D
    public void inOrder(BinaryTreeImpl<T> a){
        if(!a.isEmpty()){
            inOrder(a.getLeft());
            System.out.println(a.getRoot());
            inOrder(a.getRight());
        }
    }

    public int sumUpTree(BinaryTreeImpl<T> t){
        return sumUpTree(t,0);
    }

    public int sumUpTree(BinaryTreeImpl<T> t, int sumResult){
        if(!t.isEmpty()){
            sumResult = sumUpTree( t.getLeft(),sumResult);
            sumResult += (Integer) t.getRoot();
            sumResult = sumUpTree( t.getRight(),sumResult);
        }
        return sumResult;
    }


    public int sumUpMultiplesOf3(BinaryTreeImpl<T> t){

        return sumUpMultiplesOf3(t,0);
    }

    public int sumUpMultiplesOf3(BinaryTreeImpl<T> t, int sumResult){
        if(!t.isEmpty()){
            sumResult = sumUpMultiplesOf3( t.getLeft(), sumResult);
            sumResult += (Integer) t.getRoot() % 3 == 0 ? (Integer) t.getRoot() : 0;
            sumResult = sumUpMultiplesOf3( t.getRight(), sumResult);
        }
        return sumResult;
    }

    /**
     * if trees are isomorfic and similar they are equal
     * @param t1
     * @param t2
     * @return whether the trees are equal or not.
     */
    public boolean equalTrees(BinaryTreeImpl<T> t1, BinaryTreeImpl<T> t2){
        return isomorficTrees(t1,t2) && similarTrees(t1,t2);
    }


    /**
     * gets the arrayList of all the values in the tree, sorts them and compare the two lists.
     * @param t1
     * @param t2
     * @return whether the trees are similiar or not.
     */
    public boolean similarTrees(BinaryTreeImpl<T> t1, BinaryTreeImpl<T> t2){
        List list1 = inOrder(t1,new ArrayList<>());
        Collections.sort(list1);
        List list2 = inOrder(t2,new ArrayList<>());
        Collections.sort(list2);
        return list1.equals(list2);
    }


    /**
     * loops the tree and makes ArrayList of values starting from the left side to the root to the right side.
     * @param a
     * @param ar
     * @return ArrayList with tree values.
     */
    public ArrayList<T> inOrder(BinaryTreeImpl<T> a, ArrayList<T> ar){
        if(!a.isEmpty()){
            inOrder(a.getLeft(),ar);
            ar.add(a.getRoot());
            inOrder(a.getRight(),ar);
        }
        return ar;
    }

    /**
     * Save a binary tree
     * @param a object
     * @param filename filename of the tree save file.
     * @throws IOException if file not found
     */
    public void saveTree(BinaryTreeImpl<T> a, String filename) throws IOException {
        FileOutputStream salida = new FileOutputStream(filename);
        ObjectOutputStream obj = new ObjectOutputStream(salida);
        obj.writeObject(a.getClass());
        obj.close();
    }

    /**
     * Load a binary tree
     * @param filename filename of the file
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void loadTree(String filename) throws IOException, ClassNotFoundException {
        FileInputStream salida = new FileInputStream(filename);
        ObjectInputStream obj = new ObjectInputStream(salida);
        obj = (ObjectInputStream) obj.readObject();
        obj.close();
    }


    public boolean isomorficTrees(BinaryTreeImpl<T> t1, BinaryTreeImpl<T> t2){
        return isomorficTrees(t1,t2,true);
    }

    /**
     * check if t1 and t2 are isomorfic, if they are both empty or if their endings are diferent at the same level it returns false.
     * @param t1
     * @param t2
     * @param result
     * @return whether the trees are isomorfic or not.
     */
    public boolean isomorficTrees(BinaryTreeImpl<T> t1, BinaryTreeImpl<T> t2, boolean result){
        if(!t1.isEmpty() || !t2.isEmpty() ){
            result = isomorficTrees( t1.isEmpty() ? new BinaryTreeImpl<T>() : t1.getLeft(), t2.isEmpty() ?
                    new BinaryTreeImpl<T>() : t2.getLeft(), result);;
            if((t1.isEmpty() == true && t2.isEmpty()== false) || (t1.isEmpty() == false && t2.isEmpty() == true)) {
                result = false;
            }
            result = isomorficTrees( t1.isEmpty() ? new BinaryTreeImpl<T>() : t1.getRight(), t2.isEmpty() ?
                    new BinaryTreeImpl<T>() : t2.getRight(), result);
        }
        return result;
    }

    public boolean isComplete(BinaryTreeImpl<T> t){
        return isComplete(t,true);
    }

    /**
     * check if every node is complete, if any node is incomplete it assigns result to false
     * @param t1
     * @param result
     * @return whether the tree is complete or not.
     */
    public boolean isComplete(BinaryTreeImpl<T> t1, boolean result){
        if(!t1.isEmpty()){
            isComplete(t1.getLeft(), result);
            result = t1.isComplete();
            isComplete(t1.getRight(),result);
        }
        return result;
    }

    /**
     * splits the ArrayList of all the values of the tree and compares the left side of the root with the right of the
     * tree, they will have to have the same size
     * @param t1
     * @return whether the tree is full or not
     */
    public boolean isFull(BinaryTreeImpl<T> t1){
        List list1 = inOrder(t1,new ArrayList<>());
        List list2 = list1.subList(0,list1.indexOf(t1.getRoot()));
        List list3 = list1.subList(list1.indexOf(t1.getRoot())+1, list1.size());

        return list2.size() == list3.size();
    }

    public boolean isStable(BinaryTreeImpl<T> t1){
        return isStable(t1,true);
    }

    /**
     * if previous element in the tree is less than next value assigns result to false
     * @param t1
     * @param result
     * @return whether the tree is stable or not
     */
    public boolean isStable(BinaryTreeImpl<T> t1, boolean result){
        if(t1.isEmpty()) return true;
        if(t1.getLeft()==null && t1.getRight()==null) return true;
        if(!t1.isEmpty()){
            Integer prev = (Integer) t1.getRoot();
            isStable(t1.getLeft(), result);
            result = prev > (Integer) t1.getRoot();
            isStable(t1.getRight(),result);
        }
        return result;
    }


    /**
     * check if all the nodes are inside the t2 tree
     * @param t1
     * @param t2
     * @return whether the t1 is occurred in t2
     */
    public boolean occurBinaryTree(BinaryTreeImpl<T> t1, BinaryTreeImpl<T> t2){
        if(equalTrees(t1,t2))return true;

        return occurBinaryTree(t1.getLeft(), t2)
                || occurBinaryTree(t1.getRight(), t2);
    }


    public ArrayList<T> treeBorder(BinaryTreeImpl<T> t1){
        return treeBorder(t1,new ArrayList<T>());
    }

    /**
     * get the node that has left and right element equal to null and add them to the result ArrayList
     * @param t1
     * @param ar
     * @return ArrayList with the depeest elements of the binary tree in every branch.
     */
    public ArrayList<T> treeBorder(BinaryTreeImpl<T> t1, ArrayList<T> ar) {
        if(!t1.isEmpty()){
            treeBorder(t1.getLeft(), ar);
            if(t1.getRight()==null && t1.getLeft() == null)ar.add(t1.getRoot());
            treeBorder(t1.getRight(),ar);
        }
        return ar;
    }

}
