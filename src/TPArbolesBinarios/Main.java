package TPArbolesBinarios;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Sebas Belaustegui on 4/4/17.
 */
public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Book l1 = new Book(12, "Hamlet", 230);
        Book l2 = new Book(25, "Muerte en las nubes", 321);
        Book l3 = new Book(22, "El precio de la intriga", 530);
        Book l4 = new Book(75, "Romeo y Julieta", 199);
        Book l5 = new Book(56, "La isla del tesoro", 452);
        Book l6 = new Book(64, "El anillo", 520);
        Book l7 = new Book(44, "El doctor Zhivago", 930);

        BinaryTreeImpl<Book> t1 = new BinaryTreeImpl<>(l7);
        BinaryTreeImpl<Book> t2 = new BinaryTreeImpl<>(l5);
        BinaryTreeImpl<Book> t3 = new BinaryTreeImpl<>(l6,t1,new BinaryTreeImpl<>());
        BinaryTreeImpl<Book> t4 = new BinaryTreeImpl<>(l4,t3,t2);
        BinaryTreeImpl<Book> t5 = new BinaryTreeImpl<>(l2,t4,new BinaryTreeImpl<>());
        BinaryTreeImpl<Book> t6 = new BinaryTreeImpl<>(l3,new BinaryTreeImpl<>(),t2);
        BinaryTreeImpl<Book> t7 = new BinaryTreeImpl<>(l1,t5,t6);

        TreeApi<Book> a = new TreeApi<>();


        IntegerTree t8 = new IntegerTree(3);
        IntegerTree t9 = new IntegerTree(9);
        IntegerTree t10 = new IntegerTree(5,t8,new IntegerTree(8));
        IntegerTree t11 = new IntegerTree(12,t10,t9);
        IntegerTree t16 = new IntegerTree(12,t9,t10);
        IntegerTree t12 = new IntegerTree(3,t11,new IntegerTree(7));
        IntegerTree t17 = new IntegerTree(3,t16,new IntegerTree());
        IntegerTree t13 = new IntegerTree(7, new IntegerTree(8),t9);
        IntegerTree t14 = new IntegerTree(33,t12,t13);
        IntegerTree t15 = new IntegerTree(7,t17,t13);

        System.out.println(a.sumUpTree(t14));
        System.out.println(a.equalTrees(t14,t15));
        System.out.println(a.sumUpMultiplesOf3(t14));
        System.out.println(a.similarTrees(t14,t15));
        System.out.println(a.isomorficTrees(t14,t14));
        System.out.println(a.isComplete(t14));
        System.out.println(a.size(t7));
        System.out.println("__________");
        a.inOrder(t7) ;
        System.out.println("__________");
        System.out.println(a.ocurrencies(t7, l5));
        System.out.println(a.ocurrencies(t7, new Book(100, "zzz", 200)));
        ArrayList<Book> books = new ArrayList<>();
        a.inOrder(t7, books);
        for (int i= 0; i < books.size(); i++) {
            System.out.println(books.get(i).getCode() + " " + books.get(i).getTitle());
        }
    }
}
