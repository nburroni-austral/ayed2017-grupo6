package TPArbolesBinarios;
import java.io.Serializable;

/**
 * Created by Sebas Belaustegui on 4/4/17.
 */
public class DoubleNode<T> implements Serializable{

     T elem;
     DoubleNode <T> right;
     DoubleNode<T> left;

    public DoubleNode(T o){
        elem = o;
    }
    public DoubleNode(){
    }

    public DoubleNode(T o, DoubleNode<T> left, DoubleNode<T> right){
        elem = o;
        this.right = right;
        this.left = left;
    }

    public T getElem() {
        return elem;
    }

    public DoubleNode<T> getRight() {
        return right;
    }

    public DoubleNode<T> getLeft() {
        return left;
    }

    public void setElem(T elem) {
        this.elem = elem;
    }

    public void setRight(DoubleNode<T> right) {
        this.right = right;
    }

    public void setLeft(DoubleNode<T> left) {
        this.left = left;
    }
}
