package sudokuPackage.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 * Created by JJaramil on 4/2/17.
 */


public class PuzzlePosition {

    private List guesses;
    private Random random;
    private SudokuCell sudokuCell;
    private SudokuCell[][]  position;

    public PuzzlePosition(SudokuCell sudokuCell, SudokuCell[][] position) {
        this.guesses = new ArrayList();
        this.sudokuCell = sudokuCell;
        this.position = position;
        this.random = new Random();
    }

    public void addGuess(int guess) {
        this.guesses.add(guess);
    }

    public SudokuCell getSudokuCell() {
        return sudokuCell;
    }

    public SudokuCell[][] getPosition() {
        return position;
    }

    public int getGuess() {
        List list = new ArrayList();
        for (Object number : sudokuCell.getPossibleValues()) {
            list.add(number);
        }
        for (Object number : guesses) {
            list.remove(number);
        }
        if (list.size() >= 1) {
            int index = random.nextInt(list.size());
            return (int)list.get(index);
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Puzzle position guesses: ");
        for (int i = 0; i < guesses.size(); i++) {
            builder.append(guesses.get(i));
            if (i < (guesses.size() - 1)) {
                builder.append(", ");
            }
        }
        builder.append("; ");
        builder.append(sudokuCell);
        return builder.toString();
    }
}