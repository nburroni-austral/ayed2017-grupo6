package sudokuPackage.controller;

import sudokuPackage.model.SudokuCell;
import sudokuPackage.model.SudokuPuzzle;
import sudokuPackage.view.SudokuMainWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by JJaramil on 4/4/17.
 */
public class Controller {

    private SudokuPuzzle model;
    private SudokuMainWindow frame;

    public Controller(SudokuPuzzle model) {
        this.model = model;
        frame = new SudokuMainWindow(this, model.getDrawWidth(), model.getPuzzleWidth());
        frame.getResetPuzzleButton().doClick();
        frame.getSetValuesButton().doClick();
        frame.getSetValuesButton().doClick();
        this.model.draw(frame.getSudokuPanel().getGraphics());
    }


    private int getValue(SudokuCell sudokuCell) {
        int value = 0;
        while (value == 0) {
            String inputValue = JOptionPane.showInputDialog(frame,
                    "Type a value from 1 to 9");

            if (inputValue == null) { // Cancel button
                return 0;
            }

            try {
                value = Integer.parseInt(inputValue);
                value = testValue(sudokuCell, value);
            } catch (NumberFormatException e) {
                value = 0;
            }
        }
        return value;
    }

    public void draw(Graphics g) {
        int x = 0;
        for (int i = 0; i < model.getPuzzleWidth(); i++) {
            int y = 0;
            for (int j = 0; j < model.getPuzzleWidth(); j++) {
                Rectangle r = new Rectangle(x, y, model.getDrawWidth(), model.getDrawWidth());
                model.getCells()[i][j].setBounds(r);
                model.getCells()[i][j].draw(g, x, y, model.getDrawWidth(), model.getCellPosition()[i][j]);
                y += model.getDrawWidth();
            }
            x += model.getDrawWidth();
        }
    }


    private int testValue(SudokuCell sudokuCell, int value) {
        if (value < 1 || value > 9) {
            value = 0;
        } else if (!sudokuCell.isPossibleValue(value)) {
            value = 0;
        }
        return value;
    }

    private void repaintSudokuPanel() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                frame.getSudokuPanel().paint(frame.getSudokuPanel().getGraphics());
                draw(frame.getSudokuPanel().getGraphics());
            }
        });
    }



    public class SetValuesButton implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            model.setSetValues(frame.getSetValuesButton().isSelected());
        }
    }

    public class SolveButton implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (frame.isSolveButtonFirstTime() && frame.getSolveButton().isSelected()) {
                model.run();
                repaintSudokuPanel();
                frame.setSolveButtonFirstTime(false);
                frame.getSolveButton().setSelected(false);
            }
        }
    }

    public class ResetPuzzleButton implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (frame.getResetPuzzleButton().isSelected()) {
                frame.setSolveButtonFirstTime(true);
                model.init();
                repaintSudokuPanel();
                frame.getResetPuzzleButton().setSelected(false);
                frame.getSetValuesButton().setSelected(true);
            }
        }
    }

    public class SetValueListener implements MouseListener {

        @Override
        public void mouseReleased(MouseEvent event) {

        }

        @Override
        public void mouseClicked(MouseEvent event) {

        }

        @Override
        public void mouseEntered(MouseEvent event) {

        }

        @Override
        public void mouseExited(MouseEvent event) {

        }

        @Override
        public void mousePressed(MouseEvent event) {
            if (model.isSetValues()) {
                SudokuCell sudokuCell = model.getSudokuCellLocation(event
                        .getPoint());
                if (sudokuCell != null) {
                    int value = getValue(sudokuCell);
                    if (value > 0) {
                        sudokuCell.setValue(value);
                        sudokuCell.setIsInitial(true);
                        model.removePossibleValue(sudokuCell);
                        sudokuCell.clearPossibleValues();
                        repaintSudokuPanel();
                    }
                }
            }
        }

    }


}
