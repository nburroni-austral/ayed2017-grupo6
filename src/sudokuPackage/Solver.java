package sudokuPackage;

import sudokuPackage.controller.Controller;
import sudokuPackage.model.SudokuPuzzle;

import java.awt.*;

/**
 * Created by JJaramil on 4/2/17.
 */

public class Solver  {

    static Controller controller;


    public static void main(String[] args) {
        controller = new Controller(new SudokuPuzzle());
    }

}