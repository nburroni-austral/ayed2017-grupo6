package sudokuPackage.view;
import sudokuPackage.controller.Controller;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

/**
 * Created by JJaramil on 4/2/17.
 */

public class SudokuMainWindow extends JFrame{

    protected static final Insets buttonInsets    = new Insets(10, 10, 0, 10);
    JPanel sudokuPanel;
     boolean isSolveButtonFirstTime;
     JToggleButton resetPuzzleButton;
     JToggleButton setValuesButton;
     JToggleButton solveButton;
     JPanel buttonPanel;
     Controller controller;

    public JPanel getSudokuPanel() {
        return sudokuPanel;
    }

    public SudokuMainWindow(Controller controller, int drawWidth, int puzzleWidth) {

        super("Sudoku Solver");
        this.controller = controller;
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                exitProcedure();
            }
        });

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
        sudokuPanel = new JPanel();
        int width = drawWidth * puzzleWidth + 1;
        sudokuPanel.addMouseListener(controller.new SetValueListener());
        sudokuPanel.setPreferredSize(new Dimension(width, width));
        sudokuPanel.paintComponents(sudokuPanel.getGraphics());
        mainPanel.add(sudokuPanel);

        JPanel holderPanel = new JPanel();
        holderPanel.setLayout(new FlowLayout());

        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());

        int gridy = 0;

        resetPuzzleButton = new JToggleButton("Reset Puzzle");
        resetPuzzleButton.addActionListener(controller.new ResetPuzzleButton());
        addComponent(buttonPanel, resetPuzzleButton, 0, gridy++, 1, 1, buttonInsets,
                GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL);

        setValuesButton = new JToggleButton("Set Initial Values");
        setValuesButton.addActionListener(controller.new SetValuesButton());
        addComponent(buttonPanel, setValuesButton, 0, gridy++, 1, 1, buttonInsets,
                GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL);

        solveButton = new JToggleButton("Solve Puzzle");
        solveButton.addActionListener(controller.new SolveButton());

        addComponent(buttonPanel, solveButton, 0, gridy++, 1, 1, buttonInsets,
                GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL);

        setValuesButton.setSelected(true);

        holderPanel.add(buttonPanel);
        mainPanel.add(holderPanel);

        setLayout(new FlowLayout());
        add(mainPanel);
        pack();
        setBounds(getBounds());
        setVisible(true);

    }

    public void exitProcedure() {
        dispose();
        System.exit(0);
    }

    public Rectangle getBounds() {
        Rectangle f = super.getBounds();
        Rectangle w = GraphicsEnvironment.getLocalGraphicsEnvironment()
                .getMaximumWindowBounds();
        f.x = (w.width - f.width) / 2;
        f.y = (w.height - f.height) / 2;
        return f;
    }



    public void setSolveButtonFirstTime(boolean solveButtonFirstTime) {
        isSolveButtonFirstTime = solveButtonFirstTime;
    }

    public JToggleButton getResetPuzzleButton() {
        return resetPuzzleButton;
    }

    public JToggleButton getSetValuesButton() {
        return setValuesButton;
    }

    public boolean isSolveButtonFirstTime() {
        return isSolveButtonFirstTime;
    }

    public JToggleButton getSolveButton() {
        return solveButton;
    }


    private void addComponent(Container container, Component component,
                                  int gridx, int gridy, int gridwidth, int gridheight, Insets insets,
                                  int anchor, int fill) {
        GridBagConstraints gbc = new GridBagConstraints(gridx, gridy,
                    gridwidth, gridheight, 1.0D, 1.0D, anchor, fill, insets, 0, 0);
        container.add(component, gbc);
    }

}