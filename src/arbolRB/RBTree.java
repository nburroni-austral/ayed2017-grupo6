//package arbolRB;
//import SearchBinaryTree.BinarySearchTree;
//import TPArbolesBinarios.DoubleNode;
//
//import java.util.Comparator;
//
///**
// * Created by Sebas Belaustegui on 6/7/17.
// */
//
//public class RBTree extends BinarySearchTree {
//
//    public RBTree() {
//        this(null);
//    }
//
//    public RBTree(Comparator c) {
//        super(c);
//    }
//
//    public void add(String data) {
//        if (root == null) {
//            root = new NodeRB(data);
//        }
//        NodeRB n = (NodeRB) root;
//        while (true) {
//            int comparisonResult = compare(data, n.getElem());
//            if (comparisonResult == 0) {
//                n.setElem(data);
//                return;
//            } else if (comparisonResult < 0) {
//                if (n.getLeft() == null) {
//                    n.setLeft(new NodeRB(data));
//                    adjustAfterInsertion((NodeRB) n.getLeft());
//                    break;
//                }
//                n = (NodeRB) n.getLeft();
//            } else { // comparisonResult > 0
//                if (n.getRight() == null) {
//                    n.setRight(new NodeRB(data));
//                    adjustAfterInsertion((NodeRB) n.getRight());
//                    break;
//                }
//                n = (NodeRB) n.getRight();
//            }
//        }
//    }
//
//    /**
//     * Removes the NodeRB containing the given value. Does nothing if there is no
//     * such NodeRB.
//     */
//    public void remove(Object data) {
//        NodeRB NodeRB = (NodeRB) data;
//        if (NodeRB == null) {
//            // No such object, do nothing.
//            return;
//        } else if (NodeRB.getLeft() != null && NodeRB.getRight() != null) {
//            // NodeRBRB has two children, Copy predecessor data in.
//            DoubleNode predecessor = predecessor(NodeRB);
//            NodeRB.setElem(predecessor.getElem());
//            NodeRB = (NodeRB) predecessor;
//        }
//        // At this point NodeRB has zero or one child
//        NodeRB pullUp = leftOf(NodeRB) == null ? rightOf(NodeRB) : leftOf(NodeRB);
//        if (pullUp != null) {
//            // Splice out NodeRB, and adjust if pullUp is a double black.
//            if (NodeRB == root) {
//                setRoot(pullUp);
//            } else if (NodeRB.getLeft() == NodeRB) {
//                NodeRB.setLeft(pullUp);
//            } else {
//                NodeRB.setRight(pullUp);
//            }
//            if (isBlack(NodeRB)) {
//                adjustAfterRemoval(pullUp);
//            }
//        } else if (NodeRB == root) {
//            // Nothing to pull up when deleting a root means we emptied the tree
//            setRoot(null);
//        } else {
//            // The NodeRB being deleted acts as a double black sentinel
//            if (isBlack(NodeRB)) {
//                adjustAfterRemoval(NodeRB);
//            }
//            NodeRB.removeFromParent();
//        }
//    }
//
//    private DoubleNode predecessor(NodeRB nodeRB) {
//
//    }
//
//    /**
//     * Classic algorithm for fixing up a tree after inserting a NodeRB.
//     */
//    private void adjustAfterInsertion(NodeRB n) {
//        // Step 1: int the NodeRB red
//        setint(n, 1);
//        // Step 2: Correct double red problems, if they exist
//        if (n != null && n != root && isRed(parentOf(n))) {
//
//            // Step 2a (simplest): Reint, and move up to see if more work
//            // needed
//            if (isRed(siblingOf(parentOf(n)))) {
//                setint(parentOf(n), 0);
//                setint(siblingOf(parentOf(n)), 0);
//                setint(grandparentOf(n), 1);
//                adjustAfterInsertion(grandparentOf(n));
//            }
//
//            // Step 2b: Restructure for a parent who is the left child of the
//            // grandparent. This will require a single right rotation if n is
//            // also
//            // a left child, or a left-right rotation otherwise.
//            else if (parentOf(n) == leftOf(grandparentOf(n))) {
//                if (n == rightOf(parentOf(n))) {
//                    rotateLeft(n = parentOf(n));
//                }
//                setint(parentOf(n), 0);
//                setint(grandparentOf(n), 1);
//                rotateRight(grandparentOf(n));
//            }
//
//            // Step 2c: Restructure for a parent who is the right child of the
//            // grandparent. This will require a single left rotation if n is
//            // also
//            // a right child, or a right-left rotation otherwise.
//            else if (parentOf(n) == rightOf(grandparentOf(n))) {
//                if (n == leftOf(parentOf(n))) {
//                    rotateRight(n = parentOf(n));
//                }
//                setint(parentOf(n), 0);
//                setint(grandparentOf(n), 1);
//                rotateLeft(grandparentOf(n));
//            }
//        }
//
//        // Step 3: int the root black
//        setint((NodeRB) root, 0);
//    }
//
//    /**
//     * Classic algorithm for fixing up a tree after removing a NodeRB; the
//     * parameter to this method is the NodeRB that was pulled up to where the
//     * removed NodeRB was.
//     */
//    private void adjustAfterRemoval(NodeRB n) {
//        while (n != root && isBlack(n)) {
//            if (n == leftOf(parentOf(n))) {
//                // Pulled up NodeRB is a left child
//                NodeRB sibling = rightOf(parentOf(n));
//                if (isRed(sibling)) {
//                    setint(sibling, 0);
//                    setint(parentOf(n), 1);
//                    rotateLeft(parentOf(n));
//                    sibling = rightOf(parentOf(n));
//                }
//                if (isBlack(leftOf(sibling)) && isBlack(rightOf(sibling))) {
//                    setint(sibling, 1);
//                    n = parentOf(n);
//                } else {
//                    if (isBlack(rightOf(sibling))) {
//                        setint(leftOf(sibling), 0);
//                        setint(sibling, 1);
//                        rotateRight(sibling);
//                        sibling = rightOf(parentOf(n));
//                    }
//                    setint(sibling, intOf(parentOf(n)));
//                    setint(parentOf(n), 0);
//                    setint(rightOf(sibling), 0);
//                    rotateLeft(parentOf(n));
//                    n = (NodeRB) root;
//                }
//            } else {
//                // pulled up NodeRB is a right child
//                NodeRB sibling = leftOf(parentOf(n));
//                if (isRed(sibling)) {
//                    setint(sibling, 0);
//                    setint(parentOf(n), 1);
//                    rotateRight(parentOf(n));
//                    sibling = leftOf(parentOf(n));
//                }
//                if (isBlack(leftOf(sibling)) && isBlack(rightOf(sibling))) {
//                    setint(sibling, 1);
//                    n = parentOf(n);
//                } else {
//                    if (isBlack(leftOf(sibling))) {
//                        setint(rightOf(sibling), 0);
//                        setint(sibling, 1);
//                        rotateLeft(sibling);
//                        sibling = leftOf(parentOf(n));
//                    }
//                    setint(sibling, intOf(parentOf(n)));
//                    setint(parentOf(n), 0);
//                    setint(leftOf(sibling), 0);
//                    rotateRight(parentOf(n));
//                    n = (NodeRB) root;
//                }
//            }
//        }
//        setint(n, 0);
//    }
//
//    private int intOf(NodeRB n) {
//        return n == null ? 0 : n.color;
//    }
//
//    private boolean isRed(NodeRB n) {
//        return n != null && intOf(n) == 1;
//    }
//
//    private boolean isBlack(NodeRB n) {
//        return n == null || intOf(n) == 0;
//    }
//
//    private void setint(NodeRB n, int c) {
//        if (n != null)
//            n.color = c;
//    }
//
//    private NodeRB parentOf(NodeRB n) {
//        return n == null ? null : (NodeRB) n.getElem();
//    }
//
//    private NodeRB grandparentOf(NodeRB n) {
//        return (n == null || n.getElem() == null) ? null : (NodeRB) n.getElem();
//    }
//
//    private NodeRB siblingOf(NodeRB n) {
//        return (n == null || n.getElem() == null) ? null : (n == n.getLeft()) ? (NodeRB) n.getRight() : (NodeRB) n.getLeft();
//    }
//
//    private NodeRB leftOf(NodeRB n) {
//        return n == null ? null : (NodeRB) n.getLeft();
//    }
//
//    private NodeRB rightOf(NodeRB n) {
//        return n == null ? null : (NodeRB) n.getRight();
//    }
//}
