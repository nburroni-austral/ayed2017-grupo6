package arbolRB;

import TPArbolesBinarios.DoubleNode;
import javafx.scene.Node;

/**
 * Created by Sebas Belaustegui on 6/7/17.
 */
public class NodeRB extends DoubleNode {
    int color = 0; //0 for black and 1 for red

    public NodeRB(String data) {
        super(data);
    }
}
