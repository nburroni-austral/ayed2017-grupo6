package TPHanoi;

import TpPilas.DynamicStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebas Belaustegui on 4/12/17.
 */
public class Game {

    DynamicStack<HanoiDisk> tower1;
    DynamicStack<HanoiDisk> tower2;
    DynamicStack<HanoiDisk> tower3;
    List<HanoiDisk> list = new ArrayList<>();

    int diskAmount;


    public Game(int diskAmount){
        tower1 = new DynamicStack<>();
        tower2 = new DynamicStack<>();
        tower3 = new DynamicStack<>();

        this.diskAmount = diskAmount;
        startGame();

        while (!gameOver()){
            next();
        }
    }

    private void startGame(){
        for (int i = diskAmount; i > 0; i--) {
            tower1.push(new HanoiDisk(i));
        }
    }

    public void next(){
        // Si se cambia la stack de la derecha tambien modificar la list.
    }

    private boolean gameOver(){
        int counter = 1;
        if (diskAmount == list.size()){
            for (int i = diskAmount; i > 0; i--) {
                if(list.get(i).getSize() != counter){
                    return false;
                }
                counter++;
                tower3.pop();
            }
            return true;
        }
        return false;
    }
}
