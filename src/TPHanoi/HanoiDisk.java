package TPHanoi;

/**
 * Created by Sebas Belaustegui on 4/12/17.
 */
public class HanoiDisk {
    private int size;

    public HanoiDisk(int size){
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
