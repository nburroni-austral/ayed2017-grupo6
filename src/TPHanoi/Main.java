package TPHanoi;

import java.util.Scanner;

/**
 * Created by Sebas Belaustegui on 4/12/17.
 */
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert amount of disks");
        int amount = scanner.nextInt();
        long start = System.currentTimeMillis();
        Game game = new Game(amount);
        System.out.println("The program solved the Hanoi Tower in: " + String.valueOf(System.currentTimeMillis()-start));
    }
}
