package Parcial2016;

import List.DynamicList;

/**
 * Created by: your name
 */
// The class may need to extend or implement from other software artifact

public class Letter {
    User myuser;
    String body;
    DynamicList<Comment> myforum; // List can´t be an instance of a class from API Java.
    //More attributes can be added

    public Letter(){
        //It must be assumed that this method build a letter from a valid user. The implementation is optional.
    }

    //More methods can be added
}

