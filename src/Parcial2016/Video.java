package Parcial2016;

import List.DynamicList;

/**
 * Created by: your name
 */
// The class may need to extend or implement from other software artifact

public class Video extends News {
    DynamicList<String> urls; // List can´t be an instance of a class from API Java.
    String body;
    //More attributes can be added

    public Video(){
        //It must be assumed that this method build a Video from urls that exist. The implementation is optional.
    }

    //More methods can be added
}
