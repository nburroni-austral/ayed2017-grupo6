package Parcial2016;

import List.DynamicList;

/**
 * Created by: your name
 */
// The class may need to extend or implement from other software artifact

public class Newspaper {
    //Attributes can be added

    public Newspaper(){
        //The set of objects listed below can be moved as attributes of the class.
        Author myauthors;
        DynamicList<Advertising> myadvertisings;
        Video myvideos;
        DynamicList <Letter> myletters;
        Editorial myeditorials;
        DynamicList <Article> myarticles;
        //More elements and algorithms can be added
    }

    //More methods can be added


}
