package TpSoccer;
import java.util.Scanner;

/**
 * Created by Sebas Belaustegui on 4/18/17.
 */
public class Main {

    private static int numberOfMatches;
    private static  int numberOfTeams;
    private static Tournament tournament;

    public static void main(String[] args) {
        teamAmount();
        matchesAmount();
        tournament = new Tournament();
        teamsSetUp();
        matchesSetUp();
        tournament.solve();
        tournament.printTournament();
    }

    private static void teamAmount(){
        int enteredNumber = 0;
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ingrese número de equipos: ");
            enteredNumber = scanner.nextInt();
        } catch (Exception ignored) {}
        if(enteredNumber <= 0 || enteredNumber>10){
            System.out.println("Debes introducir un número del 1 al 10.");
            teamAmount();
        }
        numberOfTeams = enteredNumber;
    }

    private static void matchesAmount(){
        int enteredNumber = 0;
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ingrese número de partidos: ");
            enteredNumber = scanner.nextInt();
        } catch (Exception ignored) {}
        if(enteredNumber <= 0 || enteredNumber>20){
            System.out.println("Debes introducir un número del 1 al 20.");
            matchesAmount();
        }
        numberOfMatches = enteredNumber;
    }

    private static void teamsSetUp(){
        Scanner scanner = new Scanner(System.in);
        for (int i = 1; i <= numberOfTeams; i++) {
            System.out.println("Equipo "+i+": ");
            Team team = new Team(scanner.next());
            System.out.println("Ingrese los puntos del equipo: ");
            team.setScore(scanner.nextInt());
            tournament.addTeam(team);
        }
    }

    private static void matchesSetUp(){
        Scanner scanner = new Scanner(System.in);
        for (int i = 1; i <= numberOfMatches; i++) {
            Match match = new Match();
            System.out.println("Ingrese los equipos del partido "+ i);
            System.out.println("Equipo 1: ");
            String team1 = scanner.next();
            for (Team a: tournament.getTeams()) {
                if(a.getName().equalsIgnoreCase(team1)){
                    match.setHome(a);
                }
            }
            if(match.getHome()==null){
                System.out.println("Hubo un error al ingresar los equipos, inténtelo de nuevo.");
                matchesSetUp();
                return;
            }
            System.out.println("Equipo 2: ");
            String team2 = scanner.next();
            for (Team a: tournament.getTeams()) {
                if(a.getName().equalsIgnoreCase(team2)){
                    match.setAway(a);
                }
            }
            if(match.getAway()==null){
                System.out.println("Hubo un error al ingresar los equipos, inténtelo de nuevo.");
                matchesSetUp();
                return;
            }
            tournament.addMatch(match);
        }
    }
}
