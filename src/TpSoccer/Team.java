package TpSoccer;



/**
 * Created by Sebas Belaustegui on 4/18/17.
 */
public class Team {

    private String name;
    private int score;
    private int numberOfGamesPlayed;

    public Team(String name){
        this.name = name;
        score = 0;
    }

    public int getNumberOfGamesPlayed() {
        return numberOfGamesPlayed;
    }

    void addPoints(int points){
        this.score += points;
    }


    void addPlayedGame(){
        numberOfGamesPlayed += 1;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    @Override
    public boolean equals(Object o){
        Team t = (Team) o;
        return this.getName().equals(t.getName());
    }

    public void setScore(int score){
       this.score = score;
    }

}
