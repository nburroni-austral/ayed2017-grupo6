package TpSoccer;

import java.util.ArrayList;

/**
 * Created by Sebas Belaustegui on 4/18/17.
 */
public class Tournament {

    private ArrayList<Team> teams;
    private ArrayList<Match> matches;

    Tournament(){
        teams = new ArrayList<>();
        matches = new ArrayList<>();
    }

    void addTeam(Team team){
        teams.add(team);
    }

    void addMatch(Match match){
        matches.add(match);
    }

    ArrayList<Team> getTeams() {
        return teams;
    }

    void printTournament(){
        StringBuilder result = new StringBuilder();
        System.out.println(" ");
        System.out.println(" ");
        for (Match match : matches) {
            if(match.getResult() == 1) result.append(" " + 2);
            if(match.getResult() == 0) result.append(" " + "X");
            if(match.getResult() == -1) result.append(" " + 1);
        }
        System.out.println("Resultados: "+result);
    }

    boolean checkScores(ArrayList<Team> teamsAux){
        boolean result = false;
        for (Team team : teamsAux) {
            if(team.getScore() <= getTeams(team).getScore()){
                result = true;
            }else{
                return false;
            }
        }
        return result;
    }

    boolean solve(){
        ArrayList<Team> teamsAux = new ArrayList<>();
        return solve(teamsAux,1,0);
    }

    boolean solve(ArrayList<Team> teamsAux,int result, int index){
            boolean answer = false;
            matches.get(index).setResult(result);
            Team away = new Team(matches.get(index).getAway().getName());
            Team home = new Team(matches.get(index).getHome().getName());
            if(!teamsAux.contains(away))teamsAux.add(away);
            if(!teamsAux.contains(home))teamsAux.add(home);
            if (result == 1) {
                getTeamsAux(teamsAux, away).addPoints(3);
                getTeamsAux(teamsAux, home).addPoints(0);
            }else if(result == 0 ){
                getTeamsAux(teamsAux, away).addPoints(1);
                getTeamsAux(teamsAux, home).addPoints(1);
            }else {
                getTeamsAux(teamsAux, away).addPoints(0);
                getTeamsAux(teamsAux, home).addPoints(3);
            }

            if(checkScores(teamsAux) && index<matches.size()-1){
                solve(teamsAux,1,index+1);
            }else if(!checkScores(teamsAux) && result == 1 && index<=matches.size()-1){
                getTeamsAux(teamsAux, away).addPoints(-3);
                getTeamsAux(teamsAux, home).addPoints(0);
                solve(teamsAux,0,index);
            }else if(!checkScores(teamsAux) && result == 0 && index<=matches.size()-1){
                getTeamsAux(teamsAux, away).addPoints(-1);
                getTeamsAux(teamsAux, home).addPoints(-1);
                solve(teamsAux,-1,index);
            }else if(checkScores(teamsAux) && index == matches.size()-1){
                answer = true;
            }
        return answer;
    }

    Team getTeamsAux(ArrayList<Team> teams, Team team){
        for (Team team1 : teams) {
            if(team1.equals(team)){
                return team1;
            }
        }
        return null;
    }

    Team getTeams(Team team1){
        for (Team team : teams) {
            if(team.equals(team1)){
                return team;
            }
        }
        return null;
    }

}
