package TpSoccer;

/**
 * Created by Sebas Belaustegui on 4/18/17.
 */
public class Match {

    private Team home;
    private Team away;
    private int result;


    Match(){
        this.home = null;
        this.away = null;
    }

    int getResult(){
        return result;
    }

    void setResult(int result) {
        this.result = result;
    }

    void setHome(Team home) {
        this.home = home;
    }

    void setAway(Team away) {
        this.away = away;
    }

    Team getHome() {
        return home;
    }

    Team getAway() {
        return away;
    }
}
