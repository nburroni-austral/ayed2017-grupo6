package tp10;

import List.DynamicList;
import tpListas.BusLine;
import tpListas.TransportOffice;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 * Created by JJaramil on 5/30/17.
 */
public class Ejercicio2 {


    public static void main(String[] args) {
        System.out.println("Loading...");
        saveFile();
        menu();
    }

    static void menu() {
        System.out.println("-------------------------------------- \n");
        System.out.println("Ingrese el nombre del archivo a buscar.");
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.next();
        fileName = loadFile(fileName);
        System.out.println("Ingrese el carácter a buscar.");
        Character character = scanner.next().substring(0,1).toCharArray()[0];
        int result = countCharacterOcurrences(fileName,character);
        System.out.println(result);
    }

    public static int countCharacterOcurrences(String file, Character character){
        int counter = 0;
        for( int i=0; i<file.length(); i++ ) {
            if( file.charAt(i) == character ) {
                counter++;
            }
        }
        return counter;
    }
    public static void saveFile() {
        ObjectOutputStream oos;
        FileOutputStream fout;
        try {
            fout = new FileOutputStream("obj.txt");
            oos = new ObjectOutputStream(fout);
            oos.writeObject("jfoejrfojeoojrnoiengonddddddjdjjjjjjjejejejejejejejoinoijjjjj");
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String loadFile(String fileName){
        ObjectInputStream objectinputstream;
        FileInputStream  streamIn;
        String readCase = "not Found";
        try {
            streamIn = new FileInputStream(fileName+".txt");
            objectinputstream = new ObjectInputStream(streamIn);
            readCase = (String) objectinputstream.readObject();
            objectinputstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return readCase;
    }
}
