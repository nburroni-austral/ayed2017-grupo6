package tp10;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by JJaramil on 5/30/17.
 */
public class Ejercicio5 {
    public static void main(String[] args) throws IOException {
        saveFile();
        separateCountryFiles("jose.txt", 70000000);
    }

    public static void saveFile(){
        ObjectOutputStream oos;
        FileOutputStream fout;
        try{
            fout = new FileOutputStream("jose.txt");
            oos = new ObjectOutputStream(fout);
            ArrayList<CountryLog> list = new ArrayList<>();
            list.add(new CountryLog("1.Argentina", "040000000","050000000"));
            list.add(new CountryLog("2.Mexico", "070000000","050500070"));
            list.add(new CountryLog("3.Haiti", "000500000","000300000"));
            list.add(new CountryLog("4.Brasil", "075000000","080000000"));
            oos.writeObject(list);
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static ArrayList<CountryLog> loadFile(String filename){
        ObjectInputStream objectinputstream;
        ArrayList<CountryLog> readCase = new ArrayList<>();
        try {
            FileInputStream streamIn = new FileInputStream(filename);
            objectinputstream = new ObjectInputStream(streamIn);
            readCase = (ArrayList<CountryLog>) objectinputstream.readObject();
            objectinputstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return readCase;
    }


    public static void separateCountryFiles(String filename, int limit) throws IOException {
        ArrayList<CountryLog> file = loadFile(filename);
        ArrayList<CountryLog> result1 = new ArrayList<>();
        ArrayList<CountryLog> result2 = new ArrayList<>();
        for (CountryLog f : file) {
            if(Integer.parseInt(f.getPib()) < limit){
                result1.add(f);
            }else {
                result2.add(f);
            }
        }
        ObjectOutputStream oos;
        FileOutputStream fout;
        try{
            fout = new FileOutputStream("<"+limit+"M.txt");
            oos = new ObjectOutputStream(fout);
            for (CountryLog a:result1) {
                oos.writeObject(a);
            }
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ObjectOutputStream oos2;
        FileOutputStream fout2;
        try{
            fout2 = new FileOutputStream(">"+limit+"M.txt");
            oos2 = new ObjectOutputStream(fout2);
            for (CountryLog a:result2) {
                oos2.writeObject(a);
            }
            oos2.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
