package tp10;

import java.io.Serializable;

/**
 * Created by JJaramil on 5/30/17.
 */
public class CountryLog implements Serializable {
    String name;
    String population;
    String pib;

    public CountryLog(String name, String population, String pib) {
        this.name = name;
        this.population = population;
        this.pib = pib;
    }

    public String getName() {
        return name;
    }

    public String getPopulation() {
        return population;
    }

    public String getPib() {
        return pib;
    }
}
