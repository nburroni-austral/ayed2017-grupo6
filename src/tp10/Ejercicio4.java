package tp10;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebas Belaustegui on 5/30/17.
 */
public class Ejercicio4 {
    public static void main(String[] args) throws IOException {
        saveFile();
        separateCountryFiles("pepe.txt");
    }

    public static void saveFile(){
        ObjectOutputStream oos;
        FileOutputStream fout;
        try{
            fout = new FileOutputStream("pepe.txt");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(
                    "1.Argentina           \n" +
                    "040000000\n" +
                    "050000000\n"+
                    "2.Mexico              \n" +
                    "070000000\n" +
                    "050500070\n"+
                    "3.Haiti               \n" +
                    "000500000\n" +
                    "000300000\n"+
                    "4.Brasil              \n" +
                    "075000000\n" +
                    "080000000\n");
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String loadFile(String filename){
        ObjectInputStream objectinputstream;
        String readCase = "";
        try {
            FileInputStream streamIn = new FileInputStream(filename);
            objectinputstream = new ObjectInputStream(streamIn);
            readCase = (String) objectinputstream.readObject();
            System.out.println("Texto:" + readCase);
            objectinputstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return readCase;
    }

    public static String[] readLines(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        List<String> lines = new ArrayList<String>();
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            lines.add(line);
        }
        bufferedReader.close();
        return lines.toArray(new String[lines.size()]);
    }

    public static void separateCountryFiles(String filename) throws IOException {
        String[] file = readLines(filename);
        ArrayList<String> result1 = new ArrayList<>();
        ArrayList<String> result2 = new ArrayList<>();
        for (int i = 1; i < file.length; i=i+3) {
            if(Integer.valueOf(file[i]) < 30000000){
                result1.add(file[i]);
            }else {
                result2.add(file[i]);
            }
        }
        ObjectOutputStream oos;
        FileOutputStream fout;
        try{
            fout = new FileOutputStream("<30M.txt");
            oos = new ObjectOutputStream(fout);
            for (String a:result1) {
                oos.writeObject(a);
            }
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ObjectOutputStream oos2;
        FileOutputStream fout2;
        try{
            fout2 = new FileOutputStream(">30M.txt");
            oos2 = new ObjectOutputStream(fout2);
            for (String a:result2) {
                oos2.writeObject(a);
            }
            oos2.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}