package tp10;

import List.DynamicList;
import tpListas.BusLine;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 * Created by Sebas Belaustegui on 5/28/17.
 */
public class Ejercicio1 {


    public static void main(String[] args) {
        saveFile();
        Scanner input = new Scanner(System.in);
        System.out.println("Ingrese un comando, L para contar lineas o C para contar caracteres.");
        String a = input.next();
        if(a.toCharArray()[0]=='L'){
            System.out.println("Ingrese el nombre del archivo a contar líneas:");
            System.out.println("Total de lineas: "+linesCounter(input.next()));
        }else if(a.toCharArray()[0]=='C'){
            System.out.println("Ingrese el nombre del archivo a contar caracteres:");
            System.out.println("Total de caracteres: "+characterCounter(input.next()));
        }else{
            System.out.println("Ingrese un caracter válido.");
        }
    }

    public static int linesCounter(String filename){
        String file = loadFile(filename);
        int result = 0;
        for (int i = 0; i < file.length(); i++) {
            if(file.charAt(i)=='\n'){
                result++;
            }
        }
        return result+1;
    }

    public static int characterCounter(String filename){
        String file = loadFile(filename);
        return file.length();
    }

    public static void saveFile(){
        ObjectOutputStream oos;
        FileOutputStream fout;
        try{
            fout = new FileOutputStream("pepe.txt");
            oos = new ObjectOutputStream(fout);
            oos.writeObject("kgkjgkjhhldsjjdfgjfkjghkg\n" +
                    "ljeljgerlgjerlkghelrkger\n" +
                    "hrgerjgkerjgekrjhglerhg\n" +
                    "kjgnkerjgkejrghek");
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String loadFile(String filename){
        ObjectInputStream objectinputstream;
        String readCase = "";
        try {
            FileInputStream streamIn = new FileInputStream(filename);
            objectinputstream = new ObjectInputStream(streamIn);
            readCase = (String) objectinputstream.readObject();
            System.out.println("Texto:" + readCase);
            objectinputstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return readCase;
    }
}
