package tp10;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 * Created by JJaramil on 5/30/17.
 */
public class Ejercicio3 {
    public static void main(String[] args) {
        System.out.println("Loading...");
        saveFile("ibwfwouhwoeuirhoweiuhroweuhrouwehrowehowehrouwehroweuhroweuhroweuhroewuhr", "obj");
        menu();
    }

    static void menu() {
        System.out.println("-------------------------------------- \n");
        System.out.println("Ingrese el nombre del archivo a buscar.");
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.next();
        String file = loadFile(fileName);
        System.out.println("Ingrese 1 para mayúsculas y 2 para minúsculas.");
        int optn = scanner.nextInt();
        toUpperLower(optn,file, fileName);
    }

    private static void toUpperLower(int option, String text, String fileName){
        if(option==1){
            text = text.toUpperCase();
            saveFile(text, fileName+"UpperCase");
            System.out.println(loadFile(fileName+"UpperCase"));
        }else{
            text = text.toLowerCase();
            saveFile(text, fileName+"LowerCase");
            System.out.println(loadFile(fileName+"UpperCase"));
        }
    }


    public static void saveFile(String file, String fileName) {
        ObjectOutputStream oos;
        FileOutputStream fout;
        try {
            fout = new FileOutputStream(fileName+".txt");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(file);
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String loadFile(String fileName){
        ObjectInputStream objectinputstream;
        FileInputStream streamIn;
        String readCase = "not Found";
        try {
            streamIn = new FileInputStream(fileName+".txt");
            objectinputstream = new ObjectInputStream(streamIn);
            readCase = (String) objectinputstream.readObject();
            objectinputstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return readCase;
    }
}
