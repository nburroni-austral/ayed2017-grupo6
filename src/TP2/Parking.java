package TP2;

import java.lang.reflect.Array;

/**
 * Created by SebasBelaustegui on 3/15/17.
 */
public class Parking {

    int PARKING_CAPACITY = 15;
    Car[] carsParked ;

    public Parking() {
        carsParked = new Car[PARKING_CAPACITY];
    }

    void parkCar(int i, Car car){
        carsParked[i] = car;
    }

    void removeCar(int i){
        carsParked[i] = null;
    }

    boolean isCarParked(Car car){
        for (int i = 0; i < carsParked.length; i++) {
            if(carsParked[i]==car){
                return true;
            }
        }
        return false;
    }

    int getCarSpot(Car car){
        for (int i = 0; i < carsParked.length; i++) {
            if(carsParked[i]==car){
                return i;
            }
        }
        return -1;
    }

    boolean isEmpty(){
        for (int i = 0; i < carsParked.length; i++) {
            if(carsParked[i]!=null){
                return false;
            }
        }
        return true;
    }

    int getAmountOfCars(){
        int cars = 0;
        for (int i = 0; i < carsParked.length; i++) {
            if(carsParked[i]!=null){
                cars++;
            }
        }
        return cars;
    }

}
