package tpListas;

import java.util.Scanner;

/**
 * Created by Sebas Belaustegui on 5/10/17.
 */
public class main {

    static TransportOffice office;

    public static void main(String[] args) {
        System.out.println("Loading...");
        office = new TransportOffice();
        menu();
    }

    static void menu(){
        System.out.println("-------------------------------------- \n");
        System.out.println("Ingrese el número de la acción que desea hacer.");
        Scanner scanner = new Scanner(System.in);
        System.out.println(
                "1. Agregar una nueva línea de colectivo. \n" +
                "2. Agregar un colectivo. \n" +
                "3. Eliminar un colectivo. \n" +
                "4. Obtener un informe ordenado por número de línea y para una misma línea por número de interno. \n" +
                "5. Informar cuantos colectivos aptos para discapacitados hay por línea. \n" +
                "6. Informar cuantos colectivos con más de 27 asientos hay por línea. \n" +
                "7. Guardar la lista en un archivo. \n" +
                "8. Recuperar la lista de un archivo. \n" +
                "9. Salir."
        );
        int a = scanner.nextInt();
        if(a == 1){
            System.out.println("Ingrese el número para la nueva linea: ");
            if(office.addBusLine(scanner.nextInt())){
                System.out.println("La linea se agregó correctamente.");
                menu();
            }else{
                System.out.println("Hubo un error al agregar la linea, revise los datos e inténtelo nuevamente.");
                menu();
            }
        }else if(a == 2){
            System.out.println("Ingrese el número de linea: ");
            int line = scanner.nextInt();
            System.out.println("Ingrese el número de interno: ");
            int intern = scanner.nextInt();
            System.out.println("Ingrese la capacidad del colectivo: ");
            int capacity = scanner.nextInt();
            System.out.println("El colectivo es apto para discapacitados? (No=0/Si=1): ");
            boolean handicapped = scanner.nextInt() == 1 ? true : false;
            if(office.addBusToLine(new Bus(intern,capacity,handicapped),line)){
                System.out.println("El colectivo se agregó correctamente.");
                menu();
            }else{
                System.out.println("Hubo un error al agregar el colectivo, revise los datos e inténtelo nuevamente.");
                menu();
            }
        }else if(a == 3){
            System.out.println("Ingrese la linea del colectivo que desea eliminar: ");
            int line = scanner.nextInt();
            System.out.println("Ingrese el interno: ");
            int intern = scanner.nextInt();
            if(office.removeBusFromLine(line,intern)){
                System.out.println("El colectivo se eliminó correctamente.");
                menu();
            }else{
                System.out.println("Hubo un error al eliminar el colectivo, revise los datos e inténtelo nuevamente.");
                menu();
            }
        }else if(a == 4){
            System.out.println(office.toString());
            menu();
        }else if(a == 5){
            System.out.println(office.getHandicapedBuses());
            menu();
        }else if(a == 6){
            System.out.println(office.getCapacityGreaterThan27());
            menu();
        }else if(a == 7) {
            office.saveFile();
            System.out.println("El archivo fue guardado con exito.");
            menu();
        }else if(a == 8) {
            office.loadFile();
            menu();
        }else if(a == 9) {
            System.exit(0);
        }

    }


}
