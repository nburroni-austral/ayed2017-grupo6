package tpListas;

import List.DynamicList;

import java.io.Serializable;

/**
 * Created by Sebas Belaustegui on 5/10/17.
 */
public class BusLine implements Serializable{

    private DynamicList<Bus> buses;
    private int lineNumber;

    BusLine(DynamicList<Bus> buses, int lineNumber) {
        buses = new DynamicList<Bus>();
        this.buses = buses;
        this.lineNumber = lineNumber;
    }

    BusLine(int lineNumber) {
        buses = new DynamicList<Bus>();
        this.lineNumber = lineNumber;
    }

    void addBus(Bus bus){
        buses.insertNext(bus);
    }

    int getLineNumber() {
        return lineNumber;
    }

    DynamicList<Bus> getBuses() {
        return buses;
    }
}
