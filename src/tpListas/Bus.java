package tpListas;

import java.io.Serializable;

/**
 * Created by Sebas Belaustegui on 5/10/17.
 */
public class Bus implements Serializable {

    private int intern;
    private int capacity;
    private boolean handicapped;

    public Bus(int intern, int capacity, boolean handicapped) {
        this.intern = intern;
        this.capacity = capacity;
        this.handicapped = handicapped;
    }

    public int getIntern() {
        return intern;
    }

    public int getCapacity() {
        return capacity;
    }

    public String isHandicapped() {
        if(handicapped){
            return "Si";
        }else {
            return "No";
        }
    }
}
