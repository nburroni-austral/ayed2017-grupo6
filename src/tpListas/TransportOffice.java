package tpListas;
import List.DynamicList;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Sebas Belaustegui on 5/10/17.
 */
public class TransportOffice {

    private DynamicList<BusLine> busLines;

    TransportOffice() {
        busLines = new DynamicList<BusLine>();
    }

     boolean addBusLine(int number){
         for (int i = 0; i < busLines.size(); i++) {
             busLines.goTo(i);
             if(busLines.getActual().getLineNumber() == number){
                 return false;
             }
         }
         busLines.insertNext(new BusLine(number));
         return true;
     }

     boolean addBusToLine(Bus bus, int number){
         for (int i = 0; i < busLines.size(); i++) {
             busLines.goTo(i);
             if(busLines.getActual().getLineNumber() == number){
                 busLines.getActual().addBus(bus);
                 return true;
             }
         }
         return false;
     }

    boolean removeBusFromLine(int line, int intern){
        for (int i = 0; i < busLines.size(); i++) {
            busLines.goTo(i);
            if(busLines.getActual().getLineNumber() == line){
                for (int j = 0; j < busLines.getActual().getBuses().size(); j++) {
                    busLines.getActual().getBuses().goTo(j);
                    if(busLines.getActual().getBuses().getActual().getIntern() == intern){
                        busLines.getActual().getBuses().remove();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public String toString(){
        String report = new String("");
        for (int i = 0; i < busLines.size() ; i++) {
            busLines.goTo(i);
            report += "\nLinea: " + busLines.getActual().getLineNumber() + "\n     Internos: ";
            for (int j = 0; j < busLines.getActual().getBuses().size(); j++) {
                busLines.getActual().getBuses().goTo(j);
                report += "         " + busLines.getActual().getBuses().getActual().getIntern() + ": \n" +"             " +
                        "Capacidad: "+    busLines.getActual().getBuses().getActual().getCapacity() + "         " +
                        "Discapacitados: " + busLines.getActual().getBuses().getActual().isHandicapped() + "\n";
            }
        }
        return report;
    }

    public String getHandicapedBuses(){
        String report = new String("");
        int amountOfHandicapedBuses = 0;
        for (int i = 0; i < busLines.size() ; i++) {
            busLines.goTo(i);
            report += "\nLinea: " + busLines.getActual().getLineNumber() + "\n     Cantidad de colectivos para discapacitados: \n";

            for (int j = 0; j < busLines.getActual().getBuses().size(); j++) {
                busLines.getActual().getBuses().goTo(j);
                if(busLines.getActual().getBuses().getActual().isHandicapped().equals("Si")) amountOfHandicapedBuses+=1;
            }
        }
        report += amountOfHandicapedBuses;
        return report;
    }

    public String getCapacityGreaterThan27(){
        String report = new String("");
        int amountOfBigBuses = 0;
        for (int i = 0; i < busLines.size() ; i++) {
            busLines.goTo(i);
            report += "\nLinea: " + busLines.getActual().getLineNumber() + "\n     Cantidad de colectivos para discapacitados: \n";

            for (int j = 0; j < busLines.getActual().getBuses().size(); j++) {
                busLines.getActual().getBuses().goTo(j);
                if(busLines.getActual().getBuses().getActual().getCapacity()>=27) amountOfBigBuses+=1;
            }
        }
        report += amountOfBigBuses;
        return report;
    }

    public void saveFile(){
        ObjectOutputStream oos;
        FileOutputStream fout;
        try{
            fout = new FileOutputStream("obj.txt");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(busLines);
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void loadFile(){
        ObjectInputStream objectinputstream;
        try {
            FileInputStream streamIn = new FileInputStream("obj.txt");
            objectinputstream = new ObjectInputStream(streamIn);
            DynamicList<BusLine> readCase = (DynamicList<BusLine>) objectinputstream.readObject();
            busLines = readCase;
            System.out.println(toString());
            //System.out.println(recordList.get(i));
            objectinputstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
