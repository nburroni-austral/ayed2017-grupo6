//package SearchBinaryTree;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.LinkedList;
//import java.util.List;
//
///**
// * Created by Sebas Belaustegui on 4/24/17.
// */
//public class Local {
//
//    BinarySearchTree<Lamp> lamps;
//
//    public Local(){
//        lamps = new BinarySearchTree<>();
//    }
//
//    public void addLamp(Lamp l){
//        lamps.insert(l);
//    }
//
//    public void removeLamp(Lamp l){
//        lamps.remove(l);
//    }
//
//    public void modifyLamp(Lamp l, int quantity ){
//        lamps.search(l).setStock(quantity);
//    }
//
//
//    /**
//     * Inserts every element of the linked list in the binary search tree.
//     * @param list
//     */
//    public void admitList(LinkedList<Lamp> list){
//        for (Lamp l: list) {
//            lamps.insert(l);
//        }
//    }
//
//    /**
//     * makes arraylist of elements in the binary search tree.
//     * @param a
//     * @param ar
//     * @return Arraylist of tree elements.
//     */
//    public ArrayList<Lamp> inOrder(BinarySearchTree<Lamp> a, ArrayList<Lamp> ar){
//        if(!a.isEmpty()){
//            inOrder(a.getLeft(),ar);
//            ar.add(a.getRoot());
//            inOrder(a.getRight(),ar);
//        }
//        return ar;
//    }
//
//    /**
//     * return sorted List of lamps in the binary search tree, by their code
//     * @return sorted List of lamps in local.
//     */
//    public List<Lamp> getOrderedLampByCode(){
//        List list1 = inOrder(lamps ,new ArrayList<>());
//        Collections.sort(list1);
//        return list1;
//    }
//}
