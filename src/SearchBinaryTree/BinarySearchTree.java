package SearchBinaryTree;

import TPArbolesBinarios.DoubleNode;

import java.util.Comparator;

//package SearchBinaryTree;
//import TPArbolesBinarios.BinaryTreeImpl;
//import TPArbolesBinarios.DoubleNode;
//
//import java.util.Comparator;
//
///**
// * Created by Sebas Belaustegui on 4/24/17.
// */
public class BinarySearchTree<T> implements Comparator<T> {

    public DoubleNode root;

    public BinarySearchTree(Comparator c) {

    }

    public BinarySearchTree() {

    }

    public void BinarySearchTree(){
        root = null;
    }

    public boolean isEmpty(){
        return (root == null);
    }

    /**
     * checks if has root.
     * @return root if there is root.
     */
    public T getRoot(){
        try{
            return (T)root.getElem();
        }catch (Exception e){
            throw new RuntimeException("El arbol esta vacio.");
        }
    }

    /**
     * checks if the left side is not empty, and returns element.
     * @return element in the left side of tree
     */
    public BinarySearchTree<T> getLeft(){
        try{
            BinarySearchTree<T> t = new BinarySearchTree<T>();
            t.root = root.getLeft();
            return t;
        }catch (Exception e){
            throw new RuntimeException("El lado izquierdo esta vacío.");
        }
    }

    /**
     * checks if the right side is not empty, and returns element.
     * @return element in the right side of tree
     */
    public BinarySearchTree<T> getRight(){
        try{
            BinarySearchTree<T> t = new BinarySearchTree<T>();
            t.root = root.getRight();
            return t;
        }catch (Exception e){
            throw new RuntimeException("El lado derecho esta vacío.");
        }
    }

    /**
     * checks if the element x exist in the binary search tree
     * @param x
     * @return true if exists, false if it doesn't.
     */
    public boolean exits(Comparable x){
        try{
            return exits(root, x);
        }catch (Exception e){
            throw new RuntimeException("El elemento no existe");
        }
    }

    /**
     * search of the minimum element of the tree.
     * @return the minimum element of tree.
     */
    public T getMin(){
        try{
            return (T)getMin(root).getElem();
        }catch (Exception e){
            throw new RuntimeException("El arbol esta vacio.");
        }
    }

    /**
     * search of the greatest element in the tree.
     * @return greatest element in the tree.
     */
    public T getMax(){
        return (T)getMax(root).getElem();
    }

    /**
     * search of the element x in the tree.
     * @param x
     * @return element
     */
    public T search(Comparable x){
        try{
            return (T)search(root, x).getElem();
        }catch (Exception e){
            throw new RuntimeException("No pertenece el elemento al arbol.");
        }
    }


    /**
     * inserts the element in the tree according to the behaviour of the binary search tree.
     * @param x
     */
    public void insert(Comparable x){
        try{
            root = insert(root, x);
        }catch (Exception e){
            throw new RuntimeException("No pertenece el elemento al arbol.");
        }
    }

    /**
     * removes the element in the tree according to the behaviour of the binary search tree.
     * @param x
     */
    public void remove(Comparable x){
        try{
            root = delete(root, x);
        }catch (Exception e){
            throw new RuntimeException("No pertenece el elemento al arbol.");
        }
    }

    /**
     * checks if the element x exist in the binary search tree
     * @param t
     * @param x
     * @return if the element exists or not.
     */
    private boolean exits(DoubleNode t, Comparable x) {
        if (t == null)
            return false;
        if (x.compareTo(t.getElem()) == 0)
            return true;
        else if (x.compareTo( t.getElem())< 0)
            return exits(t.getLeft(), x);
        else
            return exits(t.getRight(), x);
    }
    /**
     * search of the minimum element of the tree.
     * @return the minimum element of tree.
     */
    private DoubleNode getMin(DoubleNode t){
        if (t.getLeft() == null)
            return t;
        else
            return getMin(t.getLeft());
    }
    /**
     * search of the maximum element of the tree.
     * @return the maximum element of tree.
     */
    private DoubleNode getMax(DoubleNode t){
        if (t.getRight() == null)
            return t;
        else
            return getMax(t.getRight());
    }


    /**
     * search of the element x in the tree.
     * @param x
     * @return element
     */
    private DoubleNode search(DoubleNode t, Comparable x){
        if (x.compareTo( t.getElem())== 0)
            return t;
        else if (x.compareTo( t.getElem())< 0)
            return search(t.getLeft(), x);
        else
            return search(t.getRight(), x);
    }


    /**
     * inserts the element in the tree according to the behaviour of the binary search tree.
     * @param x
     */
    private DoubleNode insert(DoubleNode t, Comparable x) {
        if (t == null){
            t = new DoubleNode();
            t.setElem(x);
        }
        else if (x.compareTo(t.getElem()) < 0)
            t.setRight(insert(t.getLeft(), x));
        else
            t.setRight(insert(t.getRight(), x));
        return t;
    }

    /**
     * removes the element in the tree according to the behaviour of the binary search tree.
     * @param x
     * @return
     */
    private DoubleNode delete(DoubleNode t, Comparable x) {
        if (x.compareTo(t.getElem()) < 0)
            t.setLeft(delete(t.getLeft(), x));
        else if (x.compareTo(t.getElem()) > 0)
            t.setRight(delete(t.getRight(), x));
        else
        if (t.getLeft() != null && t.getRight() != null ) {
            t.setElem(getMin(t.getRight()).getElem());
            t.setRight(deleteMin(t.getRight()));
        }
        else if (t.getLeft() != null)
            t = t.getLeft();
        else
            t =t.getRight();
        return t;
    }

    /**
     * deletes the minimum element of the tree
     * @param t
     * @return removed element.
     */
    private DoubleNode deleteMin(DoubleNode t){
        if (t.getLeft() != null)
            t.setLeft(deleteMin(t.getLeft()));
        else
            t = t.getRight();
        return t;
    }

    public void inOrder(BinarySearchTree<T> a){
        if(!a.isEmpty()){
            inOrder(a.getLeft());
            System.out.println(a.getRoot());
            inOrder(a.getRight());
        }
    }

    @Override
    public int compare(T o1, T o2) {
        if(o1.equals(o2)){
            return 0;
        }else{
            return -1;
        }
    }
}
