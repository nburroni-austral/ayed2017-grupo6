package SearchBinaryTree;

/**
 * Created by Sebas Belaustegui on 4/24/17.
 */
public class Lamp<T> implements Comparable<Lamp> {

    String Code;
    int watts;
    String type;
    int stock;

    public Lamp(String code, int watts, String type, int stock) {
        Code = code;
        this.watts = watts;
        this.type = type;
        this.stock = stock;
    }

    public String getCode() {
        return Code;
    }

    public int getWatts() {
        return watts;
    }

    public String getType() {
        return type;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    /**
     * compares lamp by their code using the String compareTo
     * @param o
     * @return 1 if o is greater than, 0 if they are equal, -1 if it is smaller.
     */
    @Override
    public int compareTo(Lamp o) {
        return this.getCode().compareTo(o.getCode());
    }
}
