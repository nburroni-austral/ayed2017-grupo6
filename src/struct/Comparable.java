package struct;

public interface Comparable<T>
{
	int compareTo(Comparable<T> x);
}
