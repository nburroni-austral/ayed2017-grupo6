package struct.impl.TP1;

/**
 * Created by SebasBelaustegui on 3/13/17.
 */
public class Ejercicio3 {

    public <T> T[] merge(Comparable<T> array1[], Comparable<T> array2[]){
        T[] array3 = (T[]) new Object[array1.length+array2.length];

        int a = 0;
        int b;

        if(array1.length > array2.length) {
            b=a+1;
            for (a = 0; a < array1.length; a++) {
                if(a<array2.length) {
                    if (array2[a].compareTo((T) array1[a]) < 0) {
                        array3[b - 1] = (T) array2[a];
                        array3[b] = (T) array1[a];
                        b += 2;

                    } else {
                        array3[b - 1] = (T) array1[a];
                        array3[b] = (T) array2[a];
                        b += 2;
                    }
                }else {
                    array3[b-1]= (T) array1[a];
                    b++;
                }

            }
        }else{
            b=a+1;
            for (a = 0; a < array2.length; a++) {
                if(a<array1.length) {
                    if (array1[a].compareTo((T) array2[a]) < 0) {
                        array3[b - 1] = (T) array1[a];
                        array3[b] = (T) array2[a];
                        b += 2;

                    } else {
                        array3[b - 1] = (T) array2[a];
                        array3[b] = (T) array1[a];
                        b += 2;
                    }
                }else {
                    array3[b-1]= (T) array2[a];
                    b++;
                    }

                }
            }
        return array3;
    }
}
