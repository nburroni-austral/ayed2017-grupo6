package struct.impl.TP1;

/**
 * Created by JJaramil on 3/13/17.
 */
public  class Ej2 {


    public  <T> void seleccion(Comparable <T> array[]){
        Comparable<T> min;
        int imin;
        for(int i =0; i < array.length-1; i ++){
            min =array[i];
            imin = i;
            for (int j = i+1; j < array.length; j++){
                if ( ( array[j]).compareTo((T) min) < 0){
                    min = array[j];
                    imin = j;
                }
            }
            array[imin]=array[i];
            array[i]= min;
        }
    }

    public  <T> void seleccionRecursive(Comparable <T> array[],int n){
        Comparable<T> min;
        int imin;
        if(n == array.length) return;
            min =array[n];
            imin = n;
            for (int j = n+1; j < array.length; j++){
                if ( ( array[j]).compareTo((T) min) < 0){
                    min = array[j];
                    imin = j;
                }
            }
            array[imin]=array[n];
            array[n]= min;
            seleccionRecursive(array, n+1);
    }

    public  void seleccion(int array[]){
        int min;
        int imin;
        for(int i =0; i < array.length-1; i ++){
            min =array[i];
            imin = i;
            for (int j = i+1; j < array.length; j++){
                if ( array[j]< min){
                    min = array[j];
                    imin = j;
                }
            }
            array[imin]=array[i];
            array[i]= min;
        }
    }

    public  void seleccion(String array[]){
        String min;
        int imin;
        for(int i =0; i < array.length-1; i ++){
            min =array[i];
            imin = i;
            for (int j = i+1; j < array.length; j++){
                if ( array[j].compareToIgnoreCase(min) < 0){
                    min = array[j];
                    imin = j;
                }
            }
            array[imin]=array[i];
            array[i]= min;
        }
    }



    public  <T> void insertionSort(Comparable<T> array[]){

        for (int i = 1; i < array.length; i++){
            Comparable<T> tmp = array[i];
            int j;

            for(j=i; j > 0 ; j--){
                    if(array[j-1].compareTo((T) tmp) < 0)break;

                    array[j] = array[j-1];
            }
            array[j] = tmp;
        }
    }
    public  void insertionSort(int array[]){

        for (int i = 1; i < array.length; i++){
            int tmp = array[i];
            int j;

            for(j=i; j > 0 ; j--){
                if(array[j-1] < tmp)break;

                array[j] = array[j-1];
            }
            array[j] = tmp;
        }
    }

    public  void insertionSort(String array[]){

        for (int i = 1; i < array.length; i++){
            String tmp = array[i];
            int j;

            for(j=i; j > 0 ; j--){
                if(array[j-1].compareToIgnoreCase(tmp)<0)break;

                array[j] = array[j-1];
            }
            array[j] = tmp;
        }
    }

    public  <T> void bubbleSort(Comparable<T> array[]){
        for (int i = array.length - 1 ; i>=0; i--){
            for (int j = 0; j <= i-1; j++){
                if (array[j].compareTo((T) array[j+1]) > 0 )
                {
                    Comparable<T> tmp = array[j];
                    array[j] = array[j + 1];
                    array[j+1] = tmp;
                }

            }
        }
    }

    public void bubbleSort(int array[]){
        for (int i = array.length - 1 ; i>=0; i--){
            for (int j = 0; j <= i-1; j++){
                if (array[j] > array[j+1] )
                {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j+1] = tmp;
                }

            }
        }
    }

    public void bubbleSort(String array[]){
        for (int i = array.length - 1 ; i>=0; i--){
            for (int j = 0; j <= i-1; j++){
                if (array[j].compareToIgnoreCase(array[j+1])>0 )
                {
                    String tmp = array[j];
                    array[j] = array[j + 1];
                    array[j+1] = tmp;
                }

            }
        }
    }



}
