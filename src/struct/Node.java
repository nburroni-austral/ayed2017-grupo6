package struct;

/**
 * Created by Lucas on 6/4/2017.
 */
public class Node <T> {
    private T object;
    Node next;

    public Node(T object) {
        this.object = object;
    }

    public T getObject() {
        return object;
    }
}