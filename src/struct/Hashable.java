package struct;

public interface Hashable
{
	int hashCode(int tableSize);
}