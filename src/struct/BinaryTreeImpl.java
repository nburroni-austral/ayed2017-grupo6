package struct;

import java.io.Serializable;

/**
 * Created by Sebas Belaustegui on 4/4/17.
 */
public class BinaryTreeImpl<T> implements Serializable{
    
    private DoubleNode<T> root;

    public BinaryTreeImpl(){
        root = null;
    }

    public BinaryTreeImpl(T o){
        root = new DoubleNode<T>(o);
    }

    public BinaryTreeImpl(T o, BinaryTreeImpl<T> tleft, BinaryTreeImpl<T> tright){
        root = new DoubleNode<T>(o,  tleft.root, tright.root);
    }

    public boolean isEmpty(){
        return root == null;
    }

    public boolean isComplete(){
           return ((!(root.getLeft() == null) && !(root.getRight() == null) ) || (root.getLeft() == null && root.getRight() == null )) ? true : false;
    }

    public T getRoot(){
        return root.getElem();
    }

    public void setRoot(DoubleNode<T> root) {
        this.root = root;
    }

    public BinaryTreeImpl<T> getLeft(){
        BinaryTreeImpl<T> t = new BinaryTreeImpl<>();
        t.root = root.getLeft();
        return t;
    }

    public BinaryTreeImpl<T> getRight(){
        BinaryTreeImpl<T> t = new BinaryTreeImpl<>();
        t.root = root.getRight();
        return t;

    }
}
