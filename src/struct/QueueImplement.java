package struct;

/**
 * Created by Sebas Belaustegui on 4/5/17.
 */
public class QueueImplement<Q> implements Queue<Q> {

    int back;
    int front;
    int amount;
    int size;
    Q[] data;

    public QueueImplement(int size){
        back = -1;
        front = -1;
        amount = 0;
        this.size = size;
        data = (Q[]) new Object[size];
    }

    public void enqueue(Q q) {
        if((back+1)%size==front) {
            throw new IllegalStateException("Queue is full.");
        }else if (isEmpty()) {
            front++;
            back++;
            data[back] = q;
            amount++;
        }else{
            back=(back+1)%size;
            data[back] = q;
            amount++;
        }
    }

    @Override
    public Q dequeue() {
        Q value;
        if (isEmpty()) {
            throw new IllegalStateException("Queue is empty, can't dequeue.");
        } else if (front == back) {
            value = data[front];
            front = -1;
            back = -1;
            amount --;
        } else {
            value = data[front];
            front=(front+1)%size;
            amount --;
        }
        return value;
    }

    @Override
    public boolean isEmpty() {
        return (front == -1 && back == -1);
    }

    @Override
    public int length() {
        return amount;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        amount=0;
        front = 0;
        back = size;
    }
}
