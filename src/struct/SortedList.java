package struct;

public interface SortedList<L> extends GeneralList<L> {
    void insert(L obj);
}
