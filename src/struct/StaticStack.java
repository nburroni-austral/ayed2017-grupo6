package struct;

/**
 * Created by JJaramil on 3/28/17.
 * Static Stack
 */
public class StaticStack <T> implements Stack<T>
{

        private int top;
        private int capacity;
        private Object[] data;

        public StaticStack(int size){
            top = -1;
            capacity = size;
            data = new Object [capacity];
        }

        public void push(T o) {
            if(top+1==data.length){
                grow();
            }
            top++;
            data[top]= o;
        }
        public void pop() {
            top--;
        }
        public boolean isEmpty() {
            if (top == -1){
                return true;
            }
            return false;
        }

         public int size() {
            return top;
        }

        public void empty() {
            top = -1;

        }
        public T peek() {
            if (!isEmpty()){
                return (T) data[top];
            }
            return null;
        }

        private void grow(){
            Object[] data2 = new Object[2*capacity];
            for (int i =0; i<capacity;i++){
                data2[i] = data[i];
            }
            data = data2;
        }
}

