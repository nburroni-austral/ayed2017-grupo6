package tp11;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Objects;

/**
 * Created by Sebas Belaustegui on 7/9/17.
 */
public class CarFile {

    private File f;
    private RandomAccessFile raf;
    private int regSize = 13;

    CarFile(String name)throws FileNotFoundException {
        f = new File(name);
        raf = new RandomAccessFile(f,"rw");
    }

    public long length()throws IOException{
        return raf.length();
    }

    long regQuantity() throws IOException {
        return (raf.length()/regSize);
    }

    void write(Car car) throws IOException {
        raf.seek(raf.length());
        raf.writeUTF(car.getLicensePlate());
        raf.writeInt(car.getType());
        raf.writeBoolean(car.isActive());
    }

    void write(Car car, long position) throws IOException {
        raf.seek(position);
        raf.writeUTF(car.getLicensePlate());
        raf.writeInt(car.getType());
        raf.writeBoolean(car.isActive());
    }

    Car read() throws IOException{
        return new Car(raf.readUTF(), raf.readInt(), raf.readBoolean());
    }

    void start() throws IOException{
        raf.seek(0);
    }

    Car find(String license) throws IOException{
        long cant = regQuantity();
        start();
        Car car;
        for (int i =0 ; i < cant;i++){
            car = read();
            if(car.isActive() && (Objects.equals(car.getLicensePlate(), license)))
                return car;
        }
        return null;
    }

    void delete(String license) throws IOException{
        Car car = find(license);
        if (car != null){
            car.setActive(false);
            write(car,raf.getFilePointer()-regSize);
            System.out.println("Eliminado correctamente.");
        }else{
            System.out.println("Vehiculo no encontrado.");
        }
    }

    void modify(String license, String newLicense, int newType) throws IOException{
        Car car = find(license);
        if (car != null){
            car.setLicensePlate(newLicense);
            car.setType(newType);
            write(car, raf.getFilePointer()-regSize);
        }
    }

    public void close() throws IOException {
        raf.close();
    }

    int countByType(int type){
        int counter = 0;
        Car car;
        try{
            long cant = regQuantity();
            for (long i = 0; i < cant ; i++){
                car = read();
                if(car.getType() == type)
                    counter++;
            }
            close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return counter;
    }

    public void list() {
        Car car;
        try {
            long cant = regQuantity();
            for (long i = 0; i < cant ; i++){
                car = read();
                if(car.isActive())
                    System.out.println(car.toString());
            }
            close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    void listByType(int type) {
        Car car;
        try {
            long cant = regQuantity();
            for (long i = 0; i < cant ; i++){
                car = read();
                if(car.getType() == type)
                    System.out.println(car.toString());
            }
            close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
