package tp11;

/**
 * Created by Sebas Belaustegui on 6/13/17.
 */
public class Car {

    private String licensePlate;
    private int type; //0 for car, 1 for pick-up, 2 for truck
    private boolean isActive; //active

    Car(String licensePlate, int type, boolean isActive) {
        this.licensePlate = licensePlate;
        this.type = type;
        this.isActive = isActive;
    }

    String getLicensePlate() {
        return licensePlate;
    }

    void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    int getType() {
        return type;
    }

    void setType(int type) {
        this.type = type;
    }

    boolean isActive() {
        return isActive;
    }

    void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        if (type == 0) {
            return "Auto patente " + licensePlate + ".";
        } else if (type == 1) {
            return "Camioneta patente " + licensePlate + ".";
        } else {
            return "Camión patente " + licensePlate + ".";
        }
    }
}
