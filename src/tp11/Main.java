package tp11;

import java.io.EOFException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Sebas Belaustegui on 7/9/17.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        printMenu();
    }

    private static void printMenu() throws IOException {
        try{
            CarFile carFile = new CarFile("Autos");
            Scanner scanner = new Scanner(System.in);
            System.out.println("\nIngrese el número de la acción que desea realizar. \n" +
                    " 1.Agregar nuevo auto. \n" +
                    " 2.Eliminar auto. \n" +
                    " 3.Modificar datos de un auto. \n" +
                    " 4.Consultar los autos. \n" +
                    " 5.Informe. \n" +
                    " 0.Salir."
            );
            switch (scanner.nextInt()) {
                case 0:
                    System.exit(0);
                case 1:
                    System.out.println("Ingrese la patente del auto (de la forma 'ABC123'): ");
                    scanner.nextLine();
                    String licensePlate = scanner.nextLine();
                    System.out.println("Ingrese el tipo de auto ('0' para auto, '1' para camioneta y '2' para camión):");
                    int type = scanner.nextInt();
                    carFile.write(new Car(licensePlate, type, true));
                    System.out.println("Agregado.");
                    carFile.close();
                    printMenu();
                    break;
                case 2:
                    System.out.println("Ingrese la patente del auto a eliminar (de la forma 'ABC123'): ");
                    scanner.nextLine();
                    String licensePlate1 = scanner.nextLine();
                    carFile.delete(licensePlate1);
                    carFile.close();
                    printMenu();
                    break;
                case 3:
                    System.out.println("Ingrese la patente del auto a modificar (de la forma 'ABC123'): ");
                    scanner.nextLine();
                    String licensePlate2 = scanner.nextLine();
                    System.out.println("Ingrese la nueva patente: ");
//                    scanner.nextLine();
                    String newLicensePlate = scanner.nextLine();
                    System.out.println("Ingrese el nuevo tipo: ");
                    int newType = scanner.nextInt();
                    carFile.modify(licensePlate2, newLicensePlate, newType);
                    System.out.println("Modificado.");
                    carFile.close();
                    printMenu();
                    break;
                case 4:
                    System.out.println("\nIngrese '1' para ver datos de un vehículo, '2' para ver la cantidad de vehículos y '3' para ver la cantidad de vehiculos por tipo.");
                    switch (scanner.nextInt()) {
                        case 1:
                            System.out.println("Ingrese patente para ver los datos (de la forma 'ABC123'): ");
                            scanner.nextLine();
                            String licensePlate3 = scanner.nextLine();
                            System.out.println(carFile.find(licensePlate3)!=null ? carFile.find(licensePlate3).toString(): "Vehículo no encontrado." );
                            printMenu();
                            break;
                        case 2:
                            System.out.println("Hay " + carFile.regQuantity() + " vehículos.");
                            printMenu();
                            break;
                        case 3:
                            System.out.println("Ingrese el tipo de vehículo('0' para auto, '1' para camioneta y '2' para camión): ");
                            System.out.println("Hay "+carFile.countByType(scanner.nextInt())+ "vehículos de ese tipo.");
                            printMenu();
                            break;
                    }
                case 5:
                    System.out.println("Ingrese '1' para listar todos vehículo, '2' para listar vehiculos por tipo: ");
                    switch (scanner.nextInt()) {
                        case 1:
                            carFile.list();
                            printMenu();
                            break;
                        case 2:
                            System.out.println("Ingrese el tipo de vehículo('0' para auto, '1' para camioneta y '2' para camión): ");
                            int type1 = scanner.nextInt();
                            carFile.listByType(type1);
                            printMenu();
                            break;
                    }
                default:
                    carFile.close();
                    printMenu();
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
