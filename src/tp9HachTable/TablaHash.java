package tp9HachTable;

import List.StaticList;
import struct.BinarySearchTree;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by JJaramil on 7/9/17.
 */
public class TablaHash {

    private StaticList t[];
    private int capacidad;
    private Soundex soundex;

    public TablaHash(int M) {
        if (!Primo.esPrimo(M))
            M = Primo.proxPrimo(M);
        capacidad = M;
        t = new StaticList[M];
        soundex = new Soundex();
        for(int i = 0; i < M ; i++)
            t[i] = new StaticList();
    }


    public void insertar (String x) {
        int k = Integer.parseInt(soundex.soundex(x).substring(1));
        //(Hashable) x).hash(capacidad);
        t[k].insertNext(x);
    }
    public Object buscar (String x) {
        int k = Integer.parseInt(soundex.soundex(x).substring(1));
        //((Hashable) x).hash(capacidad);
        t[k].goTo(0);
        int l = t[k].size();
        for (int i = 0 ; i < l ; i ++ )
            if (x.compareTo((String) t[k].getActual()) == 0)
                return t[k].getActual();
        return t[k].getActual();
    }
    public BinarySearchTree obtenerArBinBus () {
        BinarySearchTree a = new BinarySearchTree();
        for (int i = 0; i < capacidad; i++ ) {
            if (!t[i].isVoid()) {
                t[i].goTo(0);
                for (int j = 0; j < t[i].size() ; j++) {
                    a.insert((java.lang.Comparable) t[i].getActual());
                    t[i].goNext();
                    j++;
                }
            }
        }
        return a;
    }


    public void loadDictionary() throws FileNotFoundException {
            final int[] amountPassed = {0};
            BufferedReader br = new BufferedReader(new FileReader("src/dictionary"));
            final int[] total = {0};
            br.lines().forEach(s -> {
                for (String d : s.split("\n")) {
                 if(d!=null)insertar(d);
                }
            });
    }

}
