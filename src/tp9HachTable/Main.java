package tp9HachTable;

import java.util.Scanner;

/**
 * Created by JJaramil on 7/9/17.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese las palabras y presione enter:");
        String s = scanner.nextLine();
        Dictionary dictionary = new Dictionary(84095);
        String[] listOfWords = s.split("\n");
        for (String d : listOfWords) {
            if(!(dictionary.buscar(d).equals(d))){
                System.out.println("Las palabra sugerida para "+d+" es:");
                System.out.println("");
                System.out.println(dictionary.buscar(d));
            }
        }
    }
}

