package tp9HachTable;

/**
 * Created by JJaramil on 7/9/17.
 */
public interface Hashable {
    int hash(int M);
}
