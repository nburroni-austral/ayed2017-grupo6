package TPStressDeTiempo;

import org.w3c.dom.ranges.RangeException;

import java.util.Scanner;

/**
 * Created by Sebas Belaustegui on 3/29/17.
 */
public class Ejercicio3 {

    public static void main(String[] args) {
        dateDistanceCalculator();
        replay();
    }

    public static void dateDistanceCalculator(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el dia de la primer fecha:");
        int i = sc.nextInt();
        System.out.println("Ingrese el mes de la primer fecha:");
        int j = sc.nextInt();
        System.out.println("Ingrese el año de la primer fecha:");
        int k = sc.nextInt();

        System.out.println("Ingrese el dia de la segunda fecha:");
        int l = sc.nextInt();
        System.out.println("Ingrese el mes de la segunda fecha:");
        int m = sc.nextInt();
        System.out.println("Ingrese el año de la segunda fecha:");
        int n = sc.nextInt();

        if(i>0&&i<30&&l>0&&l<30&&j>0&&j<12&&m>0&&m<12){
            System.out.println();
            System.out.println("La distancia en dias entre las fechas es: "+Math.abs(i-l)+(Math.abs(j-m)*30+(Math.abs(k-n)*365)));
            System.out.println();
        }else{
            System.err.println("Ingrese fecha valida.");
        }
    }

    public static void replay(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Desea volver a hacerlo?");
        System.out.println("Si: 1 | No: 2");
        int a = sc.nextInt();
        if(a==1){
            dateDistanceCalculator();
        }else if(a==2){
            System.exit(0);
        }else{
            System.out.println("Ingrese numero valido.");
            replay();
        }
    }
}
