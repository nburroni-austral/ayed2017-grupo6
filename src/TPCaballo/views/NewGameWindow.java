package TPCaballo.views;

import TPCaballo.controller.GameController;

import javax.swing.*;
import java.awt.*;

/**
 * This Class creates the New Game Window.
 */
public class NewGameWindow extends JFrame{

    Container contents;
    private JButton[][] squares = new JButton[8][8];

    private JButton nextButton;
    private JButton backButton;

    GameController controller;

    private ImageIcon blackSquare;
    private ImageIcon whiteSquare;
    private ImageIcon blacKnight;
    private ImageIcon whiteKnight;

    private Image blackKnightImg;
    private Image whiteKnightImg;

    /**
     * New Game Window constructor.
     * @param controller game controller.
     */
    public NewGameWindow(GameController controller){

        this.controller = controller;

        contents = getContentPane();
        contents.setLayout(new GridLayout(9,8));

        try {
            blacKnight = new ImageIcon("src/icons/negro-con-caballo.png");
            whiteKnight = new ImageIcon("src/icons/caballo-en-blanco.png");
            blackSquare = new ImageIcon("src/icons/color-negro.png");
            whiteSquare = new ImageIcon("src/icons/color-blanco.png");
        }catch (Exception e){
            System.out.println("No se encontró una o más de las imágenes necesarias para la ejecución.");
        }

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                squares[i][j] = new JButton();
                if((i+j)%2 != 0){
                    squares[i][j].setIcon(blackSquare);
                }else {
                    squares[i][j].setIcon(whiteSquare);
                }
                contents.add(squares[i][j]);
            }
        }

        blackKnightImg = blacKnight.getImage().getScaledInstance( 50, 50,  java.awt.Image.SCALE_SMOOTH ) ;
        blacKnight = new ImageIcon(blackKnightImg);

        whiteKnightImg = whiteKnight.getImage().getScaledInstance( 50, 50,  java.awt.Image.SCALE_SMOOTH ) ;
        whiteKnight = new ImageIcon(whiteKnightImg);
        squares[0][0].setIcon(whiteKnight);

        backButton = new JButton("Back");
        backButton.addActionListener(controller.new BackButtonAction());
        contents.add(backButton);

        nextButton = new JButton("Next");
        nextButton.addActionListener(controller.new NextButtonAction());
        contents.add(nextButton);

        setSize(300,300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * When the horse changes it position this method is called.
     * It refresh the interface and move the horse to the new position.
     * @param a vertical index of new horse position.
     * @param b horizontal index of new horse position.
     */
    public void refreshWindow(int a, int b){

        contents.removeAll();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                squares[i][j] = new JButton();
                if (a==i && b==j && (i+j)%2 != 0){
                    squares[i][j].setIcon(blacKnight);
                }else if (a==i && b==j && (i+j)%2 == 0){
                    squares[i][j].setIcon(whiteKnight);
                }else if ((i+j)%2 != 0){
                    squares[i][j].setIcon(blackSquare);
                }else {
                    squares[i][j].setIcon(whiteSquare);
                }
                contents.add(squares[i][j]);
            }
        }
        contents.add(backButton);
        contents.add(nextButton);
        SwingUtilities.updateComponentTreeUI(this);
    }
}
