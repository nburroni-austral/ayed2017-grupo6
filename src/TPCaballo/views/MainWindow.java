package TPCaballo.views;

import TPCaballo.controller.GameController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * This Class creates the Main Window.
 */
public class MainWindow extends JFrame {

    JPanel listPane;
    JLabel title;
    JLabel members;
    JButton startGame;
    JButton exit;
    Container contentPane;
    GameController controller;

    /**
     * Main Window constructor.
     * Window cuadradoNegro: "Main Menu".
     * @param controller
     */
    public MainWindow(GameController controller){

        super("Main Menu");
        this.controller = controller;

        listPane = new JPanel();
        listPane.setLayout(new BoxLayout(listPane, BoxLayout.Y_AXIS));

        title = new JLabel("Trabajo Práctico Movimiento de Caballo");
        title.setAlignmentX(Component.CENTER_ALIGNMENT);
        listPane.add(title);

        members = new JLabel("Sebastián Beláustegui - José Jaramillo");
        members.setAlignmentX(Component.CENTER_ALIGNMENT);
        members.setBorder(new EmptyBorder(10, 10, 10, 10));
        listPane.add(members);

        startGame = new JButton("New Game");
        startGame.addActionListener(controller.new NewGameButtonAction());
        startGame.setAlignmentX(Component.CENTER_ALIGNMENT);
        listPane.add(startGame);

        exit = new JButton("Exit");
        exit.addActionListener(controller.new ExitButtonAction());
        exit.setAlignmentX(Component.CENTER_ALIGNMENT);
        exit.setMaximumSize(startGame.getMinimumSize());
        listPane.add(exit);

        contentPane = getContentPane();
        contentPane.add(listPane, BorderLayout.CENTER);

        listPane.setBorder(new EmptyBorder(20,20,20,20));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
