package TPCaballo.controller;

import java.awt.event.ActionEvent;

import TPCaballo.model.Horse;
import TPCaballo.views.MainWindow;
import TPCaballo.views.NewGameWindow;

import java.awt.event.ActionListener;

/**
 * Controller Class.
 * Has two frames:
 *      -Main Window
 *      -New Horse Window
 * Also has the button's ActionListeners used for each window.
 */

public class GameController {

     MainWindow frame;
     NewGameWindow newGameFrame;
     Horse horse;

    /**
     * Horse Controller's constructor.
     * Creates a Main Window's instance.
     */
    public GameController() {
        frame = new MainWindow(this);
    }

    /**
     * Creates a New Horse Window.
     */
     void createNewGame() {
         newGameFrame = new NewGameWindow(this);
         horse = new Horse("A1");
     }

    /**
     * Back button's ActionListener Class.
     * If newGameFrame it's created and it's visible, it means it's New Horse back button and hides it.
     * Finally it shows Main window.
     */
    public  class BackButtonAction implements  ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if(newGameFrame != null && newGameFrame.isVisible()){
                newGameFrame.setVisible(false);
            }
            frame.setVisible(true);
        }
    }

    /**
     * When exit button it's pressed the app exits.
     */
    public  class ExitButtonAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }

    /**
     * New Horse Game button's ActionListener Class.
     * It hides Main Window and if it's not instantiated it creates a new game window. If yes it is it shows the new game window.
     */
    public  class NewGameButtonAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
           frame.setVisible(false);
            if(newGameFrame != null){
                newGameFrame.setVisible(true);
            }else {
                createNewGame();
            }
        }
    }

    /**
     * When next button is pressed the horse is randomly moved.
     * Also calls the refresh Main window method to refresh the interface.
     */
    public  class NextButtonAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i <= (int)(Math.random()*horse.getPossibleMoves().size()); i++) {
                horse.getPossibleMoves().pop();
            }
            String moveTo = horse.getPossibleMoves().peek();
            horse.move(moveTo);
            newGameFrame.refreshWindow(moveTo.charAt(1)-49,Integer.valueOf(moveTo.charAt(0))-65);
        }
    }

}

