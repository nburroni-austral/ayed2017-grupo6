package TPCaballo.model;

/**
 * Horse entity class.
 */
public class Horse {

    private StaticStack<String> possibleMoves;
    private String position;

    /**
     * Horse constructor
     * @param position from where it start.
     */
    public Horse(String position) {
        possibleMoves = new StaticStack<>(5);
        this.position = position;
        fillStack();
    }

    /**
     * Fills the possible moves stack with current horse position.
     */
    private void fillStack() {
        int possibleMovesAmount = 0;
        possibleMoves.push(position);
        System.out.println("Possible moves are:");
        for (int i = (int)'A'; i < (int)'H' && possibleMovesAmount < 8; i++) {
            for (int j = 1; j < 8 && possibleMovesAmount < 8; j++) {
                if ((Math.abs(i - (int)position.charAt(0)) == 2) && (Math.abs(j - Integer.parseInt(position.substring(1))) == 1) || (Math.abs(i - (int) position.charAt(0)) == 1) && (Math.abs(j - Integer.parseInt(position.substring(1))) == 2)) {
                    System.out.println("-"+String.valueOf((char)i) + String.valueOf(j));
                    possibleMoves.push(String.valueOf((char)i) + String.valueOf(j));
                    possibleMovesAmount++;
                }
            }
        }
    }

    /**
     * Print form where to where the horse moved.
     * Reset the possible moves Stack with new position.
     * @param place
     */
    public void move(String place){
        System.out.println("Horse moved from '"+ this.position +"' to '" + place + "'.");
        position = place;
        possibleMoves.empty();
        possibleMoves.push(place);
        fillStack();
    }

    /**
     * @return String's Stack of horse possible moves.
     */
    public StaticStack<String> getPossibleMoves() {
        return possibleMoves;
    }
}
