package TPCaballo;

import TPCaballo.controller.GameController;

/**
 * Main Class.
 * Creates a Controller's instance.
 */

public class Main {

    static GameController controller;

    public static void main(String[] args) {
        controller = new GameController();
    }

}
