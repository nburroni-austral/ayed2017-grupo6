package TpBank;

/**
 * Created by Sebas Belaustegui on 4/25/17.
 */
public class Account implements Comparable<Account> {

    char local; // Sucursal A y sucursal B.
    int id;

    Account(char local, int id){
        this.local = local;
        this.id = id;
    }

    public char getLocal() {
        return local;
    }

    public int getId() {
        return id;
    }

    @Override
    public int compareTo(Account o) {
        if(this.id == o.getId()){
            return 0;
        }else if(this.id >= o.getId()){
            return -1;
        }else{
            return 1;
        }
    }

    @Override
    public String toString(){
        return "El cliente " + id + " del local " + local;
    }
}
