//package TpBank;
//
//import SearchBinaryTree.BinarySearchTree;
//import TPArbolesBinarios.BinaryTreeImpl;
//import TPArbolesBinarios.TreeApi;
//
//import java.util.ArrayList;
//
///**
// * Created by Sebas Belaustegui on 4/25/17.
// */
//public class Bank {
//
//    private BinaryTreeImpl<Account> accounts;
//    BinarySearchTree<Account> accountsA = new BinarySearchTree<>();
//    BinarySearchTree<Account> accountsB = new BinarySearchTree<>();
//
//    Bank(){
//        accounts = new BinaryTreeImpl<>();
//    }
//
//    void binarySearchTreeConstructor(){
//        TreeApi<Account> api = new TreeApi<>();
//        ArrayList<Account> accountsList = api.inOrder(accounts,new ArrayList<>());
//        for (Account a: accountsList) {
//            if(a.getLocal() == 'A'){
//                accountsA.insert(a);
//            }else {
//                accountsB.insert(a);
//            }
//        }
//    }
//
//    void setAccounts(BinaryTreeImpl<Account> accounts) {
//        this.accounts = accounts;
//    }
//
//    void printClientsOfLocalA(){
//        System.out.println("Clientes local A.");
//        if(!accountsA.isEmpty()){
//            printClientsOfLocalA(accountsA.getLeft());
//            System.out.println(accountsA.getRoot());
//            printClientsOfLocalA(accountsA.getRight());
//        }
//    }
//
//    void printClientsOfLocalA(BinarySearchTree<Account> a){
//        if(!a.isEmpty()){
//            printClientsOfLocalA(a.getLeft());
//            System.out.println(a.getRoot());
//            printClientsOfLocalA(a.getRight());
//        }
//    }
//
//    void printClientsOfLocalB(){
//        System.out.println("Clientes local B.");
//        if(!accountsB.isEmpty()){
//            printClientsOfLocalB(accountsB.getLeft());
//            System.out.println(accountsB.getRoot());
//            printClientsOfLocalB(accountsB.getRight());
//        }
//    }
//
//    void printClientsOfLocalB(BinarySearchTree<Account> b){
//        if(!b.isEmpty()){
//            printClientsOfLocalB(b.getLeft());
//            System.out.println(b.getRoot());
//            printClientsOfLocalB(b.getRight());
//        }
//    }
//
//
//
//}
