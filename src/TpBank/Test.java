//package TpBank;
//
//import TPArbolesBinarios.BinaryTreeImpl;
//
///**
// * Created by Sebas Belaustegui on 4/25/17.
// */
//public class Test {
//
//    public static void main(String[] args) {
//        Bank bank = new Bank();
//
//        Account l1 = new Account('A',12);
//        Account l2 = new Account('B',25);
//        Account l3 = new Account('B',22);
//        Account l4 = new Account('A',75);
//        Account l5 = new Account('A',56);
//        Account l6 = new Account('B',64);
//        Account l7 = new Account('B',44);
//
//        BinaryTreeImpl <Account> t1 = new BinaryTreeImpl<>(l7);
//        BinaryTreeImpl <Account> t2 = new BinaryTreeImpl<>(l5);
//        BinaryTreeImpl <Account> t3 = new BinaryTreeImpl<>(l6,t1,new BinaryTreeImpl<>());
//        BinaryTreeImpl <Account> t4 = new BinaryTreeImpl<>(l4,t3,t2);
//        BinaryTreeImpl <Account> t5 = new BinaryTreeImpl<>(l2,t4,new BinaryTreeImpl<>());
//        BinaryTreeImpl <Account> t6 = new BinaryTreeImpl<>(l3,new BinaryTreeImpl<>(),t2);
//        BinaryTreeImpl <Account> t7 = new BinaryTreeImpl<>(l1,t5,t6);
//
//        bank.setAccounts(t7);
//        bank.binarySearchTreeConstructor();
//        bank.printClientsOfLocalA();
//        bank.printClientsOfLocalB();
//    }
//}
