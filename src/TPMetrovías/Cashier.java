package TPMetrovías;

import Queue.DynamicQueue;
import Queue.Queue;

/**
 * Created by Sebas Belaustegui on 4/19/17.
 */
public class Cashier {

    private Queue<Client> clientQueue;
    private double income;
    private int idleTime;
    private boolean isInactive;
    private int clientWaitTime;
    private int clientsAttended;

    Cashier(){
        clientQueue = new DynamicQueue<Client>();
        income = 0;
        idleTime = 0;
        isInactive = true;
    }

    double checkout(){
        return income;
    }

    void addClientToQueue(Client client){
        clientQueue.enqueue(client);
        isInactive = false;
    }

    void removeClientOfQueue(int currentTime){
        if (!clientQueue.isEmpty()) {
            clientWaitTime += (currentTime - clientQueue.dequeue().getTimeOfArrive());
            clientsAttended += 1;
            income += 0.7;
        }
        if (clientQueue.isEmpty()){
            isInactive = true;
        }
    }

    public int getClientAverageWaitingTime(){
        return clientWaitTime/clientsAttended;
    }

    public int getIdleTime() {
        return idleTime;
    }

    boolean isInactive() {
        return isInactive;
    }

    void addIdleTime() {
        idleTime++;
    }

    Queue<Client> getClientQueue() {
        return clientQueue;
    }
}
