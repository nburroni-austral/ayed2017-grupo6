package TPMetrovías;

import java.util.Scanner;

/**
 * Created by Sebas Belaustegui on 4/19/17.
 */
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el número de cajeros que quiere simular: ");
        Controller metrovías = new Controller(scanner.nextInt());
        metrovías.simulation();
    }
}
