package TPMetrovías;

/**
 * Created by Sebas Belaustegui on 4/19/17.
 */
public class Client {

    private int timeOfArrive;
    private boolean isWaiting;

    Client(int timeOfArrive){
       this.timeOfArrive = timeOfArrive;
       isWaiting = true;
    }

    int getTimeOfArrive() {
        return timeOfArrive;
    }

    int getWaitingTime(int currentTime){
        return currentTime-timeOfArrive;
    }

}
