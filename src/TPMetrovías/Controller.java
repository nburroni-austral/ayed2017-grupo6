package TPMetrovías;

import List.DynamicList;
import struct.List;

/**
 * Created by Sebas Belaustegui on 4/19/17.
 */
public class Controller {

    private int currentTime; //Current time goes from 0 (in seconds, represents open time 6hs) to 57570 (in seconds, represents close time 21:59:30hs).
    private List<Cashier> cashierList;

    Controller(int numberOfCashiers){
        cashierList = new DynamicList<Cashier>();
        currentTime = 0;
        for (int i = 0; i < numberOfCashiers; i++) {
            cashierList.insertNext(new Cashier());
        }
    }

    void simulation(){
        while (currentTime <= 57570){
            if(currentTime%10 == 0){
                for (int i = 0; i < 5; i++) {
                    cashierList.goTo((int)Math.floor(Math.random()*cashierList.size()));
                    cashierList.getActual().addClientToQueue(new Client(currentTime));
                }
                for (int i = 0; i < cashierList.size(); i++) {
                    if(Math.random()<=0.3){
                        cashierList.goTo(i);
                        cashierList.getActual().removeClientOfQueue(currentTime);
                    }
                }
            }
            for (int i = 0; i < cashierList.size(); i++) {
                cashierList.goTo(i);
                if(cashierList.getActual().isInactive()){
                    cashierList.getActual().addIdleTime();
                }
            }
            currentTime++;

        }
        attendLastClients();
        soutIncome();
        soutIdleTime();
        soutAverageWaitTime();
    }

    private void attendLastClients(){
        for (int i = 0; i < cashierList.size(); i++) {
            cashierList.goTo(i);
            for (int j = 0; j <= cashierList.getActual().getClientQueue().size(); j++) {
                cashierList.getActual().removeClientOfQueue(currentTime);
            }
        }
    }

    private void soutAverageWaitTime(){
        for (int i = 0; i < cashierList.size(); i++) {
            cashierList.goTo(i);
            System.out.println("El tiempo de espera de la ventanilla "+(i+1)+" es "+cashierList.getActual().getClientAverageWaitingTime());
        }
    }
    private void soutIncome(){
        for (int i = 0; i < cashierList.size(); i++) {
            cashierList.goTo(i);
            System.out.println("El cajero "+ (i+1) +" recaudó $" + cashierList.getActual().checkout() + ".");
        }
    }

    private void soutIdleTime(){
        for (int i = 0; i < cashierList.size(); i++) {
            cashierList.goTo(i);
            System.out.println("El cajero " + (i+1) + " flojeó " + cashierList.getActual().getIdleTime() + " segundos.");
        }
    }
}
